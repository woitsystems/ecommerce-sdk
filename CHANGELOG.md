# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.0] - 2025-02-26
# Added
- Added `enableCustomBoxes` param on `Model\Shop`
- Added `boxes` param on `Model\Rate\RatesItemRequest`
- Added `boxes` param on `Model\RateRequest`
- Added `packing` param on `Model\RateRequest`
- Added `packing` param on `Model\Rate\RatesRequest`
- Added model `Model\Rate\PackingRequest`
- Added model `Model\Rate\PackingItemRequest`
- Added `packing` method on `Endpoint\Rates`

## [1.3.13] - 2024-08-21
# Fixed
- Fix minor bugs

## [1.3.12] - 2024-06-11
# Added
- Added `export` and `import` methods in `Endpoint\Shops\Products`
- Added `getConfig` and `updateConfig` methods in `Endpoint\Shops`

## [1.3.11] - 2024-05-29
# Fixed
- Fixed `addCountry` method on `Model\Carrier`
# Added
- Added `install` and `uninstall` methods in `Endpoint\Shops`

## [1.3.10] - 2024-03-15
# Fixed
- Fixed set param `boxes` on action `bulkUpdate` in `Endpoint\Shop\Products`

## [1.3.9] - 2024-02-01
# Added
- Added `enableFrontend` and `enableBackend` params on `Model\Carrier\Config`

## [1.3.8] - 2024-01-10
# Added
- Added `deleted` param on `Model\Shop\Box`

## [1.3.7] - 2023-12-27
# Added
- Added `residentalAddress` param on `Model\Shop`
- Added `residentalAddress` param on `Model\Rate/RateExternal`
- Added `residental` param on `Model\RateRequest`
- Added `residental` param on `Model\Rate\RatesRequest`

## [1.3.6] - 2023-12-22
# Added
- Added `provinces` method on `WorldOptions`

## [1.3.5] - 2023-12-05
# Added
- Added `labels` and `label` method on `Endpoint\Shops\Shipments`

## [1.3.4] - 2023-11-09
# Added
- Added `Model\Localization\Province` and `Endpoint\Localization\Provinces`
- Added `province` param on `Model\Shop\Address`
- Added `provinces` param on `Model\Localization\Country`

## [1.3.3] - 2023-11-08
# Added
- Added `thermalPrinterSettings` param on `Model\Shop`

## [1.3.2] - 2023-11-06
# Added
- Added Dev Mode
- Added `boxes` param on `Model\Shop\Product`

## [1.3.1] - 2023-10-03
# Added
- Added `dateFormat` and `timeFormat` param on `Model\Shop`
- Added `shippingDays` param on `Model\Shop\Product`
# Fixed
- Fixed `update()` method on `Endpoint\Shops`

## [1.3.0] - 2023-09-26
- Added `ratesTimeout` and `backupRates` param on `Model\Shop`
- Added `backupRates` param on `Model\Rate`
- Added `Model\Shop\BackupRate` class
- Added `Model\Rate\RateBackup` class
- Added `Endpoint\Shop\BackupRates` class
- Added `Endpoint\Rate\BackupRates` class
- Added `backupRates()` method on `Endpoint\Shop`
- Added `backupRates()` method on `Endpoint\Rate`

## [1.2.7] - 2023-08-18
- Added `data` param on `Model\Shop\Order`
- Change POST `/shops` endpoint to `shops/create`
- Added `contact()` method on `Endpoint\Customers`
- Fixed `update()` method on `Endpoint\Shop\Orders`

## [1.2.6] - 2023-08-11
- Added removing http(s):// and www. from site url

## [1.2.5] - 2023-07-31
- Added `displayName` param on `Model\Carrier\Config`

## [1.2.4] - 2023-07-27
- Added `shippingFeeType` and `shippingFeeAmount` params on `Model\Shop\Product`

## [1.2.3] - 2023-07-11
- Added links to documentation & samples
- Added `getById()` method on `Endpoint\Shop\Shipments`
- Removed `update()` method on `Endpoint\Shop\Shipments`
- Changed const `PLATFORM_OPENACART` to `PLATFORM_OPENCART` in `Model\Shop`

## [1.2.2] - 2023-06-30
- Fixed: Don't delete existing product dimensions when perform update method on `Endpoint\Shop\Products`
- Fixed: Check if `title` param on exception exist in `Endpoint\Endpoint`, if not then not present it

## [1.2.1] - 2023-06-27
- Added `invoiceType` and `paperlessInvoice` params to `Model\Shop`
- Added const `INVOICE_TYPES`, `INVOICE_TYPE_HELP_ME_GENERATE`, `INVOICE_TYPE_I_ALREADY_HAVE_ONE` to `Model\Shop` 
- Updated: Check if boolean params in `Model\Shop` exist, if not then set it to default values on get methods

## [1.2.0] - 2023-06-23
- Added `throwException()` method in abstract class `Endpoint\Endpoint`
- Added `showExceptions` param in class `WorldOptions` (now you decide if throw exception or return empty value, when REST API return error)
- Use `throwException()` on all `Endpoint\*` classes 
- Added `enablePacking` param in `Model\Shop` and `Model\Shop\Product`
- Present `Datetime` in correct format on `Model\Model`
- Fixed: Don't send parameteres to REST API only when param is null on `Model\Model`

## [1.1.8] - 2023-06-14
- Added `enableDeliveryDropOff` param to `Model\Shop`

## [1.1.7] - 2023-06-12
- Set `code` param only if exist in constructor `Model\Carrier`
- Added `Model\Carrier\Config` and `Endpoint\Carrier\Configs` classes
- Removed `Model\Carrier\Alias` and `Endpoint\Carrier\Aliases` classes
- Added `collectionReadyTime` and `collectionCloseTime` params to `Model\Shop`

## [1.1.6] - 2023-06-01
- Added `iossName` and `iossNumber` params to `Model\Shop`
- Changed `invoice()` to `invoices()` method in `Endpoint\Shop\Orders`

## [1.1.5] - 2023-05-29
- Added const `PLATFORM_PRESTASHOP`, `PLATFORM_WIX` to `Model\Shop` 
- Added `signature()` to `certificate()` method in `Endpoint\Shop\Shipments`

## [1.1.4] - 2023-05-05
- Fixed display error, when REST API return 400 response in `Endpoint\Shop\Shipments`
- If `vatNumber`, `eoriNumber`, `thermalPrinter` or `collectionDay` params not exist, display null on get methods in `Model\Shop` 
- If `trackingNumber` param not exist, display null on get and constructor methods in `Model\Shop\Shipment` 
- Send 0 value in REST API requests, changes in `Model\Model` 

## [1.1.3] - 2022-12-29
- Added `create()`, `update()` and `delete()` methods in `Endpoint\Shop\Product\Dimensions`
- Remove null parameters in `Mode\Model`
- If `vatNumber`, `eoriNumber`, `thermalPrinter` or `collectionDay` params not exist don't set them on constructor method in `Model\Shop` 

## [1.1.2] - 2022-10-20
- Changes in library dependencies to add PHP 7.1 compatibility 

## [1.1.1] - 2022-10-17
- Fixed file location for class `Model\Shop\CreateShipment`

## [1.1.0] - 2022-10-11
- Added `Model\Rate\RatesRequest`, `Model/Rate/RatesItemRequest`, `Model/Shop/Rate`
- Added `multi()` method in `Endpoint/Rates`
- Added `getOwn()` method in `Endpoint/Shop/Addresses`
- Added `own` param in `Model/Shop/Address`
- Remove `rate` and `service` params in `Model/Shop/Order`
- Added `rates` param in `Model/Shop/Order`
- Added `wo()` method in `Endpoint/Localization/Countries`
- Added `create`, `update()` and `delete()` methods in `Endpoint/Shop/CustomRates`
- Added `find()` and `exist()` methods in `Endpoint/Shops`
- Added `internal()` method `Endpoint/Carriers`
- Added `check()`, `bulkUpdate()` methods in `Endpoint/Shop/Products`

## [1.1.0] - 2022-03-18
- First release