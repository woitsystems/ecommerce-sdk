<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Utils;

use Psr\Http\Message\ResponseInterface;

final class Response
{
    public static function getContent(ResponseInterface $response): ?array
    {
        return json_decode($response->getBody()->getContents(), true);
    }
}