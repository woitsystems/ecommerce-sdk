<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions;

use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin\BaseUriPlugin;
use Http\Client\Common\Plugin\HeaderDefaultsPlugin;
use Http\Message\Authentication\Header;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Message\UriFactory;
use Http\Client\Common\Plugin\AuthenticationPlugin;
use WorldOptions\Endpoint\Carriers;
use WorldOptions\Endpoint\Customers;
use WorldOptions\Endpoint\Rates;
use WorldOptions\Endpoint\Shops;
use WorldOptions\Endpoint\Localization\Countries;
use WorldOptions\Endpoint\Localization\Currencies;
use WorldOptions\Endpoint\Localization\Provinces;
use WorldOptions\Endpoint\Localization\Zones;

final class WorldOptions
{
    private const API_URL = 'https://ecommerce.worldoptions.com/api';

    private const DEV_API_URL = 'https://prelive-ecommerce.worldoptions.com/api';

    /**
     * @var ClientBuilder $clientBuilder
     */
    private ClientBuilder $clientBuilder;

    /**
     * @var bool $showExceptions
     */
    private bool $showExceptions = true;

    /**
     * @var bool $devMode
     */
    private bool $devMode = false;

    /**
     * @param string $url
     * @param string $username
     * @param string $meternumber
     * @param bool $devMode
     * 
     */
    public function __construct(
        string $url,
        string $username = null,
        string $meternumber = null,
        bool $devMode = false,
        bool $showExceptions = true
    ) {
        $this->clientBuilder = new ClientBuilder();
        $this->showExceptions = $showExceptions;
        $this->devMode = $devMode;

        if ($username && $meternumber) {
            $authentication = new Header('X-AUTH-TOKEN', base64_encode(self::clearUrl($url) . ':' . $username . ':' . $meternumber));
            $this->clientBuilder->addPlugin(
                new AuthenticationPlugin($authentication)
            );
        }

        if ($this->devMode) {
            $apiUrl = self::DEV_API_URL;
        } else {
            $apiUrl = self::API_URL;
        }

        $uriFactory = Psr17FactoryDiscovery::findUriFactory();
        $this->clientBuilder->addPlugin(
            new BaseUriPlugin($uriFactory->createUri($apiUrl))
        );

        $this->clientBuilder->addPlugin(
            new HeaderDefaultsPlugin(
                [
                    'User-Agent' => 'WorldOptions PHP SDK',
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            )
        );
    }

    /**
     *
     * @return string
     */
    public static function clearUrl(string $url)
    {
        return str_replace(['www.', 'https://', 'http://'], '', $url);
    }

    /**
     * @param bool $showExceptions
     * 
     * @return self
     * 
     */
    public function setShowExceptions(bool $showExceptions): self
    {
        $this->showExceptions = $showExceptions;
        return $this;
    }

    /**
     * 
     * @return bool
     * 
     */
    public function isShowExceptions(): bool
    {
        return $this->showExceptions;
    }
    
    public function carriers(): Carriers
    {
        return new Carriers($this);
    }

    public function countries(): Countries
    {
        return new Countries($this);
    }

    public function currencies(): Currencies
    {
        return new Currencies($this);
    }

    public function customers(): Customers
    {
        return new Customers($this);
    }

    public function provinces(): Provinces
    {
        return new Provinces($this);
    }

    public function rates(): Rates
    {
        return new Rates($this);
    }

    public function shops(): Shops
    {
        return new Shops($this);
    }

    public function zones(): Zones
    {
        return new Zones($this);
    }

    /**
     * 
     * @return HttpMethodsClientInterface
     * 
     */
    public function getHttpClient(): HttpMethodsClientInterface
    {
        return $this->clientBuilder->getHttpClient();
    }
}