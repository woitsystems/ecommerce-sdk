<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier;
use WorldOptions\Model\Carrier\Config;
use WorldOptions\Endpoint\Endpoint;
use WorldOptions\Endpoint\Carrier\Services;
use WorldOptions\Endpoint\Carrier\Configs;

final class Carriers extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Carrier/operation/getCarrierItem
     * 
     * @param int $id
     * 
     * @return Carrier
     */
    public function get(int $id): ?Carrier
    {
        $response = $this->sdk->getHttpClient()->get('/carriers/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Carrier($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Carrier/operation/getCarrierCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/carriers');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $carriers = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $carriers[] = new Carrier($item);
                }
            }
            return $carriers;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Carrier/operation/internal_carriersCarrierCollection
     * 
     * @return array
     */
    public function internal(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/carriers/internal');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $carriers = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $carriers[] = new Carrier($item);
                }
            }
            return $carriers;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    public function services(): Services
    {
        return new Services($this->sdk);
    }

    public function configs(): Configs
    {
        return new Configs($this->sdk);
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Carrier/operation/get_configCarrierItem
     * 
     * @param int $id
     * 
     * @return Config|null
     */
    public function config(int $id): ?Config
    {
        $response = $this->sdk->getHttpClient()->get('/carriers/' . $id . '/config');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result ? new Config($result) : null;
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}