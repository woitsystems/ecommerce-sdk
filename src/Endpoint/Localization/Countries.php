<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Localization;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Localization\Country;
use WorldOptions\Endpoint\Endpoint;

final class Countries extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Country/operation/getCountryItem
     * 
     * @param string $code
     * 
     * @return Country
     */
    public function get(string $code): ?Country
    {
        $response = $this->sdk->getHttpClient()->get('/countries/' . $code);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Country($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Country/operation/getCountryCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/countries');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $countries = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $countries[] = new Country($item);
                }
            }
            return $countries;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Country/operation/countries_woCountryCollection
     * 
     * @return array
     */
    public function wo(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/countries/wo');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $countries = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $countries[] = new Country($item);
                }
            }
            return $countries;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}