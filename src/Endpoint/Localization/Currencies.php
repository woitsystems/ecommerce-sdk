<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Localization;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Endpoint\Endpoint;

final class Currencies extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Currency/operation/getCurrencyItem
     * 
     * @param string $code
     * 
     * @return Currency
     */
    public function get(string $code): ?Currency
    {
        $response = $this->sdk->getHttpClient()->get('/currencies/' . $code);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Currency($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Currency/operation/getCurrencyCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/currencies');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $currencies = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $currencies[] = new Currency($item);
                }
            }
            return $currencies;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}