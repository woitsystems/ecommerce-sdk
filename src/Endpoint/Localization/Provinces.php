<?php
/*
 * @since 1.3.4
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Localization;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Localization\Province;
use WorldOptions\Endpoint\Endpoint;

final class Provinces extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Province/operation/getProvinceItem
     * 
     * @param int $id
     * 
     * @return Province
     */
    public function get(int $id): ?Province
    {
        $response = $this->sdk->getHttpClient()->get('/provinces/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Province($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Province/operation/getProvinceCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/provinces');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $provinces = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $provinces[] = new Province($item);
                }
            }
            return $provinces;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}