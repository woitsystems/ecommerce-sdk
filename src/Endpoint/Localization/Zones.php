<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Localization;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Localization\Zone;
use WorldOptions\Endpoint\Endpoint;
use Exception;

final class Zones extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Zone/operation/getZoneItem
     * 
     * @param string $code
     * 
     * @return Zone
     */
    public function get(string $code): ?Zone
    {
        $response = $this->sdk->getHttpClient()->get('/zones/' . $code);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Zone($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Zone/operation/getZoneCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/zones');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $zones = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $zones[] = new Zone($item);
                }
            }
            return $zones;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}