<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint;

use WorldOptions\WorldOptions as Sdk;
use WorldOptions\Utils\ResponseException;

abstract class Endpoint
{
    public const TYPE_ARRAY = 'array';
    public const TYPE_BOOL = 'boolean';
    public const TYPE_CLASS = 'class';
    public const TYPE_STRING = 'string';

    /**
     * @var Sdk $sdk
     */
    protected Sdk $sdk;

    /**
     * @param Sdk $sdk
     */
    public function __construct(Sdk $sdk)
    {
        $this->sdk = $sdk;
    }

    /**
     * @param ?array $result
     * @param int $code
     */
    public function throwException(?array $result = [], string $type = self::TYPE_ARRAY, int $code = 0)
    {
        if ($this->sdk->isShowExceptions()) {
            if ($result && is_array($result)) {
                $message = '';
                if (isset($result['title'])) {
                    $message .= $result['title'] . ': ';
                }
                $message .= (isset($result['message']) ? $result['message'] : $result['detail']);
            } else {
                $message = 'Ocurred error, please contact with developer!';
            }
            throw new ResponseException($message, $code);
        } else {
            switch ($type) {
                case self::TYPE_ARRAY:
                    return [];
                break;
                case self::TYPE_BOOL:
                    return false;
                break;
                case self::TYPE_CLASS:
                    return null;
                break;
                case self::TYPE_STRING:
                    return '';
                break;
            }
        }
    }
}