<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint;

use WorldOptions\Utils\Response;
use WorldOptions\WorldOptions as Sdk;
use WorldOptions\Model\Shop;
use WorldOptions\Endpoint\Shop\Addresses;
use WorldOptions\Endpoint\Shop\Boxes;
use WorldOptions\Endpoint\Shop\BackupRates;
use WorldOptions\Endpoint\Shop\CustomRates;
use WorldOptions\Endpoint\Shop\Orders;
use WorldOptions\Endpoint\Shop\Products;
use WorldOptions\Endpoint\Shop\Shipments;

final class Shops extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/shop_find_by_urlShopItem
     * 
     * @param string $url
     * 
     * @return Shop|null|Exception
     */
    public function find(string $url): ?Shop
    {
        $body = [
            'url' => $url,
        ];

        $response = $this->sdk->getHttpClient()->post('/shops/find', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return new Shop($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/api_shop_existShopItem
     * 
     * @param string $url
     * 
     * @return bool
     */
    public function exist(string $url): bool
    {
        $body = [
            'url' => $url,
        ];

        $response = $this->sdk->getHttpClient()->post('/shops/exist', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['exist'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/postShopCollection
     * 
     * @param Shop $shop
     * 
     * @return Shop
     */
    public function create(Shop $shop): ?Shop
    {
        $body = [
            'url' => $shop->getUrl(),
            'platform' => $shop->getPlatform(),
        ];

        if($shop->getCustomer()) {
            $body['customer'] = [
                'username' => $shop->getCustomer()->getUsername(),
                'password' => $shop->getCustomer()->getPassword(),
                'meternumber' => $shop->getCustomer()->getMeternumber(),
                'country' => $shop->getCustomer()->getCountry()->getCode(),
            ];
        }

        if($shop->getWeightUnit()) {
            $body['weightUnit'] = $shop->getWeightUnit();
        }

        if($shop->getLengthUnit()) {
            $body['lengthUnit'] = $shop->getLengthUnit();
        }

        if($shop->getDateFormat()) {
            $body['dateFormat'] = $shop->getDateFormat();
        }

        if($shop->getTimeFormat()) {
            $body['timeFormat'] = $shop->getTimeFormat();
        }

        $body['residentalAddress'] = $shop->isResidentalAddress();
        $body['enableRates'] = $shop->isEnableRates();
        $body['enableAutobook'] = $shop->isEnableAutobook();
        $body['displayTrackingNumber'] = $shop->isDisplayTrackingNumber();
        $body['devMode'] = $shop->isDevMode();

        if($shop->getVatNumber()) {
            $body['vatNumber'] = $shop->getVatNumber();
        }

        if($shop->getEoriNumber()) {
            $body['eoriNumber'] = $shop->getEoriNumber();
        }

        if($shop->getIossName()) {
            $body['iossName'] = $shop->getIossName();
        }

        if($shop->getIossNumber()) {
            $body['iossNumber'] = $shop->getIossNumber();
        }

        if($shop->getWeightUnit()) {
            $body['defaultWidth'] = $shop->getDefaultWidth();
        }

        if($shop->getDefaultLength()) {
            $body['defaultLength'] = $shop->getDefaultLength();
        }

        if($shop->getDefaultDepth()) {
            $body['defaultDepth'] = $shop->getDefaultDepth();
        }

        if($shop->getDefaultWeight()) {
            $body['defaultWeight'] = $shop->getDefaultWeight();
        }

        if($shop->getPackageType()) {
            $body['packageType'] = $shop->getPackageType();
        }
        
        if($shop->getDutiesPayer()) {
            $body['dutiesPayer'] = $shop->getDutiesPayer();
        }
        if($shop->getCollectionType()) {
            $body['collectionType'] = $shop->getCollectionType();
        }
        if($shop->getCollectionDay()) {
            $body['collectionDay'] = $shop->getCollectionDay();
        }
        if($shop->getInvoiceType()) {
            $body['invoiceType'] = $shop->getInvoiceType();
        }

        $body['paperlessInvoice'] = $shop->isPaperlessInvoice();
        $body['addTaxToRates'] = $shop->isAddTaxToRates();
        $body['showDeliveryDate'] = $shop->isShowDeliveryDate();

        if($shop->getThermalPrinter()) {
            $body['thermalPrinter'] = $shop->getThermalPrinter();
        }

        if($shop->getThermalPrinterSettings()) {
            $body['thermalPrinterSettings'] = $shop->getThermalPrinterSettings();
        }

        if($shop->getAddress()) {
            $body['address'] = [
                'name' => $shop->getAddress()->getName(),
                'company' => $shop->getAddress()->getCompany(),
                'firstname' => $shop->getAddress()->getFirstname(),
                'lastname' => $shop->getAddress()->getLastname(),
                'street' => $shop->getAddress()->getStreet(),
                'postcode' => $shop->getAddress()->getPostcode(),
                'city' => $shop->getAddress()->getCity(),
                'state' => $shop->getAddress()->getState(),
                'country' => $shop->getAddress()->getCountry()->getCode(),
                'phone' => $shop->getAddress()->getPhone(),
                'email' => $shop->getAddress()->getEmail(),
            ];
        }

        if($shop->getCollectionAddress()) {
            $body['collectionAddress'] = [
                'name' => $shop->getCollectionAddress()->getName(),
                'company' => $shop->getCollectionAddress()->getCompany(),
                'firstname' => $shop->getCollectionAddress()->getFirstname(),
                'lastname' => $shop->getCollectionAddress()->getLastname(),
                'street' => $shop->getCollectionAddress()->getStreet(),
                'postcode' => $shop->getCollectionAddress()->getPostcode(),
                'city' => $shop->getCollectionAddress()->getCity(),
                'state' => $shop->getCollectionAddress()->getState(),
                'country' => $shop->getCollectionAddress()->getCountry()->getCode(),
                'phone' => $shop->getCollectionAddress()->getPhone(),
                'email' => $shop->getCollectionAddress()->getEmail(),
            ];
        }

        if($shop->getRatesTimeout()) {
            $body['ratesTimeout'] = $shop->getRatesTimeout();
        }

        $response = $this->sdk->getHttpClient()->post('/shops/create', [], json_encode($body));

        $result = Response::getContent($response);
        if ($response->getStatusCode() == 201) {  
            return new Shop($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/shop_authorizeShopItem
     * 
     * @param Shop $shop
     * 
     * @return bool
     */
    public function authorize(Shop $shop): bool
    {
        $body = [
            'url' => $shop->getUrl(),
            'platform' => $shop->getPlatform(),
            'customer' => [
                'username' => $shop->getCustomer()->getUsername(),
                'password' => $shop->getCustomer()->getPassword(),
                'meternumber' => $shop->getCustomer()->getMeternumber(),
                'country' => $shop->getCustomer()->getCountry()->getCode(),
            ]
        ];

        $response = $this->sdk->getHttpClient()->post('/shops/authorize', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['success'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/patchShopItem
     * 
     * @param Shop $shop
     * 
     * @return Shop
     */
    public function update(Shop $shop): ?Shop
    {
        $data = $shop->__toArray();

        if (isset($data['countries'])) {
            $countries = [];
            foreach ($shop->getCountries() as $country) {
                $countries[] = $country->getCode();
            }
            $data['countries'] = $countries;
        }

        if (isset($data['services'])) {
            unset($data['services']);
            $services = [];
            foreach ($shop->getCarrierServices() as $service) {
                $services[] = $service->getId();
            }
            $data['carrierServices'] = $services;
        }

        if ($shop->getAddress()) {
            $data['address'] = $shop->getAddress()->getId();
        }

        if ($shop->getCollectionAddress()) {
            $data['collectionAddress'] = $shop->getCollectionAddress()->getId();
        }

        $response = $this->sdk->getHttpClient()->patch('/shops/' . $shop->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], json_encode($data));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shop($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/getShopItem
     * 
     * @param int $id
     * 
     * @return Shop
     */
    public function get(int $id): ?Shop
    {
        $response = $this->sdk->getHttpClient()->get('/shops/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shop($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/shop_get_infoShopItem
     * 
     * @return Shop
     */
    public function info(): ?Shop
    {
        $response = $this->sdk->getHttpClient()->get('/shops/info');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shop($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shop/operation/getShopCollection
     * 
     * @param int $page
     * 
     * @return array
     */
    public function all(int $page = 1): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/shops?page=' . $page);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $shops = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $shops[] = new Shop($item);
                }
            }
            return $shops;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    public function addresses(): Addresses
    {
        return new Addresses($this->sdk);
    }

    public function boxes(): Boxes
    {
        return new Boxes($this->sdk);
    }

    public function orders(): Orders
    {
        return new Orders($this->sdk);
    }

    public function products(): Products
    {
        return new Products($this->sdk);
    }

    public function rates(): CustomRates
    {
        return new CustomRates($this->sdk);
    }

    public function backupRates(): BackupRates
    {
        return new BackupRates($this->sdk);
    }

    public function shipments(): Shipments
    {
        return new Shipments($this->sdk);
    }

    /**
     * 
     * @param string $url
     * @param string $platform
     * @param ?string $version
     * @param ?string $description
     * 
     * @return bool
     */
    public function install(string $url, string $platform, ?string $version = null, ?string $description = null): bool
    {
        $body = [
            'url' => $url,
            'platform' => $platform,
            'version' => $version,
            'description' => $description,
        ];

        $response = $this->sdk->getHttpClient()->post('/shops/install', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['success'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * 
     * @param string $url
     * @param string $platform
     * @param ?string $version
     * @param ?string $description
     * 
     * @return bool
     */
    public function uninstall(string $url, string $platform, ?string $version = null, ?string $description = null): bool
    {
        $body = [
            'url' => $url,
            'platform' => $platform,
            'version' => $version,
            'description' => $description,
        ];

        $response = $this->sdk->getHttpClient()->post('/shops/uninstall', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['success'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * 
     * @return ?array
     */
    public function getConfig(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/shops/config');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            if ($result) {
                return $result['config'];
            }
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * 
     * @param array $data
     * 
     * @return bool
     */
    public function updateConfig(array $data): bool
    {
        $response = $this->sdk->getHttpClient()->patch('/shops/config', [], json_encode($data));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['success'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }
}