<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Rate\Box;
use WorldOptions\Endpoint\Endpoint;

final class Boxes extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateBox/operation/getRateBoxItem
     * 
     * @param int $id
     * 
     * @return Box
     */
    public function get(int $id): ?Box
    {
        $response = $this->sdk->getHttpClient()->get('/rate_boxes/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Box($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}