<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Rate\Carrier as RateCarrier;
use WorldOptions\Endpoint\Endpoint;

final class Carriers extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateCarrier/operation/getRateCarrierItem
     * 
     * @param int $id
     * 
     * @return RateCarrier
     */
    public function get(int $id): ?RateCarrier
    {
        $response = $this->sdk->getHttpClient()->get('/rate_carriers/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new RateCarrier($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}