<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Rate\RateExternal;
use WorldOptions\Endpoint\Endpoint;

final class ExternalRates extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateWebservices/operation/getRateWebservicesItem
     * 
     * @param int $id
     * 
     * @return RateExternal
     */
    public function get(int $id): ?RateExternal
    {
        $response = $this->sdk->getHttpClient()->get('/rate_webservices/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new RateExternal($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}