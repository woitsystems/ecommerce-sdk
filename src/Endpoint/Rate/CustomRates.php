<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Rate\RateCustom;
use WorldOptions\Endpoint\Endpoint;

final class CustomRates extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateCustom/operation/getRateCustomItem
     * 
     * @param int $id
     * 
     * @return RateCustom
     */
    public function get(int $id): ?RateCustom
    {
        $response = $this->sdk->getHttpClient()->get('/rate_customs/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new RateCustom($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}