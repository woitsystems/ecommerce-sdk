<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier;
use WorldOptions\Model\Carrier\Service;
use WorldOptions\Model\Rate\RateInternal;
use WorldOptions\Model\Rate\Box;
use WorldOptions\Model\Rate\Item;
use WorldOptions\Model\Rate\Carrier as RateCarrier;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Endpoint\Endpoint;

final class InternalRates extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateInternal/operation/getRateInternalItem
     * 
     * @param int $id
     * 
     * @return RateInternal
     */
    public function get(int $id): ?RateInternal
    {
        $response = $this->sdk->getHttpClient()->get('/rate_internals/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new RateInternal($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}