<?php
/*
 * @since 1.2.8
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Rate;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Rate\RateBackup;
use WorldOptions\Endpoint\Endpoint;

final class BackupRates extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/RateBackup/operation/getRateBackupItem
     * 
     * @param int $id
     * 
     * @return RateBackup
     */
    public function get(int $id): ?RateBackup
    {
        $response = $this->sdk->getHttpClient()->get('/rate_backups/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new RateBackup($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
}