<?php
/*
 * @since 1.1.7
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Carrier;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier\Config;
use WorldOptions\Endpoint\Endpoint;

final class Configs extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierConfig/operation/getCarrierConfigItem
     * 
     * @param int $id
     * 
     * @return Config
     */
    public function get(int $id): ?Config
    {
        $response = $this->sdk->getHttpClient()->get('/carrier_configs/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Config($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierConfig/operation/deleteCarrierConfigItem
     * 
     * @param int $id
     * 
     * @return bool
     */
    public function delete(int $id): bool
    {
        $response = $this->sdk->getHttpClient()->delete('/carrier_configs/' . $id);
        if ($response->getStatusCode() == 204) {            
            return true;
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierConfig/operation/postCarrierConfigCollection
     * 
     * @param Config $config
     * 
     * @return Config
     */
    public function create(Config $config): ?Config
    {
        $collectionReadyTime = null;
        if ($config->getCollectionReadyTime()) {
            $collectionReadyTime = $config->getCollectionReadyTime()->format('Y-m-d H:i:s');
        }
        $collectionCloseTime = null;
        if ($config->getCollectionCloseTime()) {
            $collectionCloseTime = $config->getCollectionCloseTime()->format('Y-m-d H:i:s');
        }
        $body = json_encode([
            'displayName' => $config->isDisplayName(),
            'alias' => $config->getAlias(),
            'collectionType' => $config->getCollectionType(),
            'collectionDay' => $config->getCollectionDay(),
            'collectionReadyTime' => $collectionReadyTime,
            'collectionCloseTime' => $collectionCloseTime,
            'carrier' => $config->getCarrier()->getId(),
            'enableFrontend' => $config->isEnableFrontend(),
            'enableBackend' => $config->isEnableBackend(),
        ]);

        $response = $this->sdk->getHttpClient()->post('/carrier_configs', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return new Config($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierConfig/operation/patchCarrierConfigItem
     * 
     * @param Config $config
     * 
     * @return Config
     */
    public function update(Config $config): ?Config
    {
        $body = json_encode($config->__toArray());
        $response = $this->sdk->getHttpClient()->patch('/carrier_configs/' . $config->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {   
            return new Config($result);;
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierConfig/operation/getCarrierConfigCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/carrier_configs');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $configs = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $config = new Config($item);
                    $configs[$config->getCarrier()->getId()] = $config;
                }
            }
            return $configs;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}