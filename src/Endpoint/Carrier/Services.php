<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Carrier;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier\Service;
use WorldOptions\Endpoint\Endpoint;

final class Services extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierService/operation/getCarrierServiceItem
     * 
     * @param int $id
     * 
     * @return Service
     */
    public function get(int $id): ?Service
    {
        $response = $this->sdk->getHttpClient()->get('/carrier_services/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Service($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CarrierService/operation/getCarrierServiceCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/carrier_services');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $services = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $services[] = new Service($item);
                }
            }
            return $services;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}