<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop\Product;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\ProductDimension;
use WorldOptions\Endpoint\Endpoint;

final class Dimensions extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/ProductDimension/operation/postProductDimensionCollection
     * 
     * @param ProductDimension $dimensions
     * 
     * @return ProductDimension
     */
    public function create(ProductDimension $dimensions): ?ProductDimension
    {
        $body = json_encode([
            'width' => $dimensions->getWidth(),
            'length' => $dimensions->getLength(),
            'depth' => $dimensions->getDepth(),
            'weight' => $dimensions->getWeight(),
            'backend' => $dimensions->isBackend(),
            'product' => $dimensions->getProduct()->getId(),
        ]);

        $response = $this->sdk->getHttpClient()->post('/product_dimensions', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 201) {            
            return new ProductDimension($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/ProductDimension/operation/patchProductDimensionItem
     * 
     * @param ProductDimension $box
     * 
     * @return ProductDimension
     */
    public function update(ProductDimension $dimensions): ?ProductDimension
    {
        $body = json_encode($dimensions->__toArray());
        $response = $this->sdk->getHttpClient()->patch('/product_dimensions/' . $dimensions->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new ProductDimension($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/ProductDimension/operation/getProductDimensionItem
     * 
     * @param int $id
     * 
     * @return ProductDimension
     */
    public function get(int $id): ?ProductDimension
    {
        $response = $this->sdk->getHttpClient()->get('/product_dimensions/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new ProductDimension($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/ProductDimension/operation/deleteProductDimensionItem
     * 
     * @param int $id
     * 
     * @return bool
     */
    public function delete(int $id): bool
    {
        $response = $this->sdk->getHttpClient()->delete('/product_dimensions/' . $id);
        if ($response->getStatusCode() == 204) {            
            return true;
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }
}