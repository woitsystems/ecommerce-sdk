<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\Address;
use WorldOptions\Endpoint\Endpoint;

final class Addresses extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Address/operation/postAddressCollection
     * 
     * @param Address $address
     * 
     * @return Address
     */
    public function create(Address $address): ?Address
    {
        $body = json_encode([
            'name' => $address->getName(),
            'company' => $address->getCompany(),
            'firstname' => $address->getFirstname(),
            'lastname' => $address->getLastname(),
            'street' => $address->getStreet(),
            'postcode' => $address->getPostcode(),
            'city' => $address->getCity(),
            'state' => $address->getState(),
            'province' => $address->getProvince() ? $address->getProvince()->getId() : null,
            'country' => $address->getCountry()->getCode(),
            'phone' => $address->getPhone(),
            'email' => $address->getEmail(),
            'own' => (bool) $address->isOwn(),
        ]);
        $response = $this->sdk->getHttpClient()->post('/addresses', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Address($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Address/operation/patchAddressItem
     * 
     * @param Address $address
     * 
     * @return Address
     */
    public function update(Address $address): ?Address
    {
        $body = $address->__toArray();
        $body['province'] = $address->getProvince() ? $address->getProvince()->getId() : null;
        $body['country'] = $address->getCountry()->getCode();
        $body['own'] = (bool) $address->isOwn();
        $body = json_encode($body);

        $response = $this->sdk->getHttpClient()->patch('/addresses/' . $address->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Address($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Address/operation/getAddressItem
     * 
     * @param int $id
     * 
     * @return Address
     */
    public function get(int $id): ?Address
    {
        $response = $this->sdk->getHttpClient()->get('/addresses/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Address($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Address/operation/own_addressesAddressCollection
     * 
     * @return array
     */
    public function getOwn(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/addresses/own');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {     
            $addresses = [];
            foreach ($result as $address) {
                $addresses[] = new Address($address);
            }
            return $addresses;       
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}