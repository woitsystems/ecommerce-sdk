<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\Box;
use WorldOptions\Endpoint\Endpoint;

final class Boxes extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Box/operation/postBoxCollection
     * 
     * @param Box $box
     * 
     * @return Box
     */
    public function create(Box $box): ?Box
    {
        $body = json_encode($box->__toArray());
        $response = $this->sdk->getHttpClient()->post('/boxes', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Box($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Box/operation/patchBoxItem
     * 
     * @param Box $box
     * 
     * @return Box
     */
    public function update(Box $box): ?Box
    {
        $body = json_encode($box->__toArray());
        $response = $this->sdk->getHttpClient()->patch('/boxes/' . $box->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Box($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Box/operation/getBoxItem
     * 
     * @param int $id
     * 
     * @return Box
     */
    public function get(int $id): ?Box
    {
        $response = $this->sdk->getHttpClient()->get('/boxes/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Box($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Box/operation/getBoxCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/boxes');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $boxes = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $boxes[] = new Box($item);
                }
            }
            return $boxes;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}