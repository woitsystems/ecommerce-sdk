<?php
/*
 * @since 1.2.8
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier;
use WorldOptions\Model\Shop\BackupRate;
use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Localization\Zone;
use WorldOptions\Model\Shop\Box;
use WorldOptions\Endpoint\Endpoint;

final class BackupRates extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/BackupRate/operation/postBackupRateCollection
     * 
     * @param BackupRate $rate
     * 
     * @return BackupRate
     */
    public function create(BackupRate $rate): ?BackupRate
    {
        $body = $rate->__toArray();
        $body['currency'] = $rate->getCurrency()->getCode();
        if ($rate->getCountries()) {
            $countries = [];
            foreach ($rate->getCountries() as $country) {
                $countries[] = $country->getCode();
            }
            $body['countries'] = $countries;
        }

        $body = json_encode($body);

        $response = $this->sdk->getHttpClient()->post('/backup_rates', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new BackupRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/BackupRate/operation/patchBackupRateItem
     * 
     * @param BackupRate $rate
     * 
     * @return BackupRate
     */
    public function update(BackupRate $rate): ?BackupRate
    {
        $body = $rate->__toArray();
        $body['currency'] = $rate->getCurrency()->getCode();
        if ($rate->getCountries()) {
            $countries = [];
            foreach ($rate->getCountries() as $country) {
                $countries[] = $country->getCode();
            }
            $body['countries'] = $countries;
        }

        $body = json_encode($body);

        $response = $this->sdk->getHttpClient()->patch('/backup_rates/' . $rate->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new BackupRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
    
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/BackupRate/operation/deleteBackupRateItem
     * 
     * @param int $id
     * 
     * @return BackupRate
     */
    public function delete(int $id): bool
    {
        $response = $this->sdk->getHttpClient()->delete('/backup_rates/' . $id);
        if ($response->getStatusCode() == 204) {            
            return true;
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/BackupRate/operation/getBackupRateItem
     * 
     * @param int $id
     * 
     * @return BackupRate
     */
    public function get(int $id): ?BackupRate
    {
        $response = $this->sdk->getHttpClient()->get('/backup_rates/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new BackupRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/BackupRate/operation/getBackupRateCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/backup_rates');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $backupRates = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $backupRates[] = new BackupRate($item);
                }
            }
            return $backupRates;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}