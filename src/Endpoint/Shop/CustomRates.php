<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Carrier;
use WorldOptions\Model\Shop\CustomRate;
use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Localization\Zone;
use WorldOptions\Model\Shop\Box;
use WorldOptions\Endpoint\Endpoint;

final class CustomRates extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CustomRate/operation/postCustomRateCollection
     * 
     * @param CustomRate $rate
     * 
     * @return CustomRate
     */
    public function create(CustomRate $rate): ?CustomRate
    {
        $body = $rate->__toArray();
        $body['currency'] = $rate->getCurrency()->getCode();
        if ($rate->getCountries()) {
            $countries = [];
            foreach ($rate->getCountries() as $country) {
                $countries[] = $country->getCode();
            }
            $body['countries'] = $countries;
        }

        $body = json_encode($body);

        $response = $this->sdk->getHttpClient()->post('/custom_rates', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new CustomRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CustomRate/operation/patchCustomRateItem
     * 
     * @param CustomRate $rate
     * 
     * @return CustomRate
     */
    public function update(CustomRate $rate): ?CustomRate
    {
        $body = $rate->__toArray();
        $body['currency'] = $rate->getCurrency()->getCode();
        if ($rate->getCountries()) {
            $countries = [];
            foreach ($rate->getCountries() as $country) {
                $countries[] = $country->getCode();
            }
            $body['countries'] = $countries;
        }

        $body = json_encode($body);

        $response = $this->sdk->getHttpClient()->patch('/custom_rates/' . $rate->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new CustomRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }
    
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CustomRate/operation/deleteCustomRateItem
     * 
     * @param int $id
     * 
     * @return CustomRate
     */
    public function delete(int $id): bool
    {
        $response = $this->sdk->getHttpClient()->delete('/custom_rates/' . $id);
        if ($response->getStatusCode() == 204) {            
            return true;
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CustomRate/operation/getCustomRateItem
     * 
     * @param int $id
     * 
     * @return CustomRate
     */
    public function get(int $id): ?CustomRate
    {
        $response = $this->sdk->getHttpClient()->get('/custom_rates/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new CustomRate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/CustomRate/operation/getCustomRateCollection
     * 
     * @return array
     */
    public function all(): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/custom_rates');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $customRates = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $customRates[] = new CustomRate($item);
                }
            }
            return $customRates;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}