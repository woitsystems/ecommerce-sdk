<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Endpoint\Endpoint;
use WorldOptions\Endpoint\Shop\Product\Dimensions;
use SplFileInfo;
use SplFileObject;

final class Products extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/getProductItem
     *
     * @param int $id
     *
     * @return Product
     */
    public function get(int $id): ?Product
    {
        $response = $this->sdk->getHttpClient()->get('/products/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            return new Product($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/postProductCollection
     *
     * @param Product $product
     *
     * @return ?Product
     */
    public function create(Product $product): ?Product
    {
        $body = [
            'name' => $product->getName(),
            'productId' => $product->getProductId(),
            'price' => $product->getPrice(),
            'currency' => $product->getCurrency()->getCode(),
            'dimensions' => [],
        ];

        if ($product->getDescription()) {
            $body['description'] = $product->getDescription();
        }

        if ($product->getHtsNumber()) {
            $body['htsNumber'] = $product->getHtsNumber();
        }

        if ($product->getReference()) {
            $body['reference'] = $product->getReference();
        }

        if ($product->getVariantId()) {
            $body['variantId'] = $product->getVariantId();
        }

        if ($product->getType()) {
            $body['type'] = $product->getType();
        }

        if ($product->getCountry()) {
            $body['country'] = $product->getCountry()->getCode();
        }

        if ($product->getShippingDays()) {
            $body['shippingDays'] = $product->getShippingDays();
        }

        if ($product->getShippingFeeAmount()) {
            $body['shippingFeeAmount'] = $product->getShippingFeeAmount();
        }

        if ($product->getShippingFeeType()) {
            $body['shippingFeeType'] = $product->getShippingFeeType();
        }

        if ($product->isKeepFlat()) {
            $body['keepFlat'] = $product->isKeepFlat();
        }

        if ($product->isEnablePacking()) {
            $body['enablePacking'] = $product->isEnablePacking();
        }

        if ($product->getDimensions()) {
            foreach ($product->getDimensions() as $dimensions) {
                $body['dimensions'][] = $dimensions->__toArray();
            }
        }

        if ($product->getBoxes() !== null) {
            if ($product->getBoxes() === null) {
                $data['boxes'] = [];
            } else {
                $boxes = [];
                foreach ($product->getBoxes() as $box) {
                    $boxes[] = $box->getId();
                }
                $data['boxes'] = $boxes;
            }
        }

        $response = $this->sdk->getHttpClient()->post('/products', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            return new Product($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/check_productsProductCollection
     *
     * @param array $products
     *
     * @return array
     */
    public function check(array $products): ?array
    {
        $data = [];
        foreach ($products as $product) {
            $item = [
                'productId' => $product->getProductId(),
            ];

            if ($product->getVariantId()) {
                $item['variantId'] = $product->getVariantId();
            }

            $data[] = $item;
        }

        $body = ['products' => $data];

        $response = $this->sdk->getHttpClient()->post('/products/check', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $products = [];
            foreach ($result as $product) {
                $products[] = new Product($product);
            }
            return $products;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/patchProductItem
     *
     * @param Product $product
     *
     * @return Product
     */
    public function update(Product $product): ?Product
    {
        $data = $product->__toArray();
        if ($product->getCountry()) {
            $data['country'] = $product->getCountry()->getCode();
        }

        if ($product->getBoxes() !== null) {
            if ($product->getBoxes() === null) {
                $data['boxes'] = [];
            } else {
                $boxes = [];
                foreach ($product->getBoxes() as $box) {
                    $boxes[] = $box->getId();
                }
                $data['boxes'] = $boxes;
            }
        } else {
            unset($data['boxes']);
        }

        if ($product->getDimensions()) {
            foreach ($product->getDimensions() as $dims) {
                $data['dimensions'][] = $dims->__toArray();
            }
        } else {
            unset($data['dimensions']);
        }

        $body = json_encode($data);

        $response = $this->sdk->getHttpClient()->patch('/products/' . $product->getId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            return new Product($result);
            ;
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/getProductCollection
     *
     * @param int $page
     *
     * @return array
     */
    public function all(int $page = 1): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/products?page=' . $page);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $products = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $products[] = new Product($item);
                }
            }
            return $products;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    public function dimensions(): Dimensions
    {
        return new Dimensions($this->sdk);
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Product/operation/update_productsProductCollection
     *
     * @param array $products
     *
     * @return array
     */
    public function bulkUpdate(array $products): ?array
    {
        $data = [];
        foreach ($products as $product) {
            $dimensions = [];
            if ($product->getDimensions()) {
                foreach ($product->getDimensions() as $dims) {
                    $dimensions[] = $dims->__toArray();
                }
            }

            $item = [
                'name' => $product->getName(),
                'productId' => $product->getProductId(),
                'price' => $product->getPrice(),
                'currency' => $product->getCurrency()->getCode(),
            ];

            if ($product->getDescription()) {
                $item['description'] = $product->getDescription();
            }

            if ($product->getHtsNumber()) {
                $item['htsNumber'] = $product->getHtsNumber();
            }

            if ($product->getReference()) {
                $item['reference'] = $product->getReference();
            }

            if ($product->getVariantId()) {
                $item['variantId'] = $product->getVariantId();
            }

            if ($product->getType()) {
                $item['type'] = $product->getType();
            }

            if ($product->getCountry()) {
                $item['country'] = $product->getCountry()->getCode();
            }

            if ($product->getShippingFeeAmount()) {
                $item['shippingFeeAmount'] = $product->getShippingFeeAmount();
            }

            if ($product->getShippingDays()) {
                $item['shippingDays'] = $product->getShippingDays();
            }

            if ($product->getShippingFeeType()) {
                $item['shippingFeeType'] = $product->getShippingFeeType();
            }

            if ($product->getBoxes() === null) {
                $item['boxes'] = [];
            } else {
                $boxes = [];
                foreach ($product->getBoxes() as $box) {
                    $boxes[] = $box->getId();
                }
                $item['boxes'] = $boxes;
            }

            $item['keepFlat'] = $product->isKeepFlat();
            $item['enablePacking'] = $product->isEnablePacking();

            if ($dimensions) {
                $item['dimensions'] = $dimensions;
            }

            $data[] = $item;
        }

        $body = ['products' => $data];

        $response = $this->sdk->getHttpClient()->post('/products/update', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $products = [];
            foreach ($result as $product) {
                $products[] = new Product($product);
            }
            return $products;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial http://localhost:8000/api/docs?ui=re_doc#tag/Product/operation/api_export_productsProductItem
     *
     * @return array
     */
    public function export(): ?array
    {
        $response = $this->sdk->getHttpClient()->post('/products/export');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            if ($result['success']) {
                return $result['data'];
            }
            return [];
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial http://localhost:8000/api/docs?ui=re_doc#tag/Product/operation/api_import_productsProductItem
     *
     * @param $file
     *
     * @return array
     */
    public function import($file): ?array
    {
        $splFileInfo = new SplFileInfo($file['name']);
        if (!in_array($splFileInfo->getExtension(), ['csv'])) {
            return $this->throwException(['message' => 'Invalid extension. Only .csv allowed'], self::TYPE_ARRAY, 400);
        }

        $payload = chunk_split(base64_encode(file_get_contents($file["tmp_name"])));

        $response = $this->sdk->getHttpClient()->post('/products/import', [], json_encode(['payload' => $payload]));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            if ($result['success']) {
                return $result['data'];
            }
            return [];
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}
