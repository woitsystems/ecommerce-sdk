<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\Order;
use WorldOptions\Endpoint\Endpoint;

final class Orders extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/postOrderCollection
     * 
     * @param Order $order
     * 
     * @return Order
     */
    public function create(Order $order): ?Order
    {
        $body = [
            'orderId' => $order->getOrderId(),
            'reference' => $order->getReference(),
            'amount' => $order->getAmount(),
            'currency' => $order->getCurrency()->getCode(),
            'data' => $order->getData(),
        ];

        if ($order->getRates()) {
            foreach ($order->getRates() as $rate) {
                $body['rates'][] = $rate->__toArray();
            }
        }
        
        $response = $this->sdk->getHttpClient()->post('/orders', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Order($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/patchOrderItem
     * 
     * @param Order $order
     * 
     * @return Order
     */
    public function update(Order $order): ?Order
    {
        $body = json_encode([
            'reference' => $order->getReference(),
            'amount' => $order->getAmount(),
            'currency' => $order->getCurrency()->getCode(),
            'data' => $order->getData(),
        ]);
        $response = $this->sdk->getHttpClient()->patch('/orders/' . $order->getOrderId(), [
            'Content-Type' => 'application/merge-patch+json',
        ], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Order($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/deleteOrderItem
     * 
     * @param int $id
     * 
     * @return bool
     */
    public function delete(int $id): ?bool
    {
        $response = $this->sdk->getHttpClient()->delete('/orders/' . $id);
        if ($response->getStatusCode() == 204) {            
            return true;
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/getOrderItem
     * 
     * @param int $id
     * 
     * @return Order
     */
    public function get(int $id): ?Order
    {
        $response = $this->sdk->getHttpClient()->get('/orders/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Order($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/orders_invoicesOrderItem
     * 
     * @param int $id
     * 
     * @return array
     */
    public function invoices(int $id): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/orders/' . $id . '/invoices');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/orders_documentsOrderItem
     * 
     * @param int $id
     * @param string $type
     * @param string $message
     * @param string $email
     * 
     * @return bool
     */
    public function documents(int $id, string $type, string $message, string $email): bool
    {
        $body = [
            'type' => $type,
            'message' => $message,
            'email' => $email,
        ];
        
        $response = $this->sdk->getHttpClient()->post('/orders/' . $id . '/documents', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result['result'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @param int $page
     * 
     * @return array
     */
    public function all(int $page = 1): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/orders?page=' . $page);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $orders = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $orders[] = new Order($item);
                }
            }
            return $orders;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Order/operation/getOrderCollection
     * 
     * @param array $orders
     * 
     * @return ?array
     */
    public function export(array $orders, string $type = 'all'): array
    {
        $items = [];
        foreach ($orders as $order) {
            $items[] = $order->__toArray();
        }

        $body = [
            'orders' => $items,
            'type' => $type,
        ];
        
        $response = $this->sdk->getHttpClient()->post('/orders/export', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result['files'];
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }
}