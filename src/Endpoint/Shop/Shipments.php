<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint\Shop;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Shop\Shipment;
use WorldOptions\Model\Shop\CreateShipment;
use WorldOptions\Endpoint\Endpoint;

final class Shipments extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/postShipmentCollection
     * 
     * @param array $data
     * 
     * @return Shipment
     */
    public function create(CreateShipment $data): ?Shipment
    {
        $body = json_encode($data->__toArray());
        $response = $this->sdk->getHttpClient()->post('/shipments', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shipment($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/shipments_get_by_tracking_numberShipmentItem
     * 
     * @param string $trackingNumber
     * 
     * @return Shipment
     */
    public function get(string $trackingNumber): ?Shipment
    {
        $response = $this->sdk->getHttpClient()->get('/shipments/' . $trackingNumber . '/get');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shipment($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * 
     * @param string $trackingNumber
     * 
     * @return Shipment
     */
    public function labels(string $trackingNumber): ?Shipment
    {
        $response = $this->sdk->getHttpClient()->get('/shipments/' . $trackingNumber . '/labels');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shipment($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * 
     * @param int $id
     * 
     * @return mixed
     */
    public function label(int $id)
    {
        $response = $this->sdk->getHttpClient()->get('/shipments/' . $id . '/label');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result;
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/getShipmentItem
     * @param int $id
     * 
     * @return Shipment
     */
    public function getById(int $id): ?Shipment
    {
        $response = $this->sdk->getHttpClient()->get('/shipments/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shipment($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/shipments_cancel_by_tracking_numberShipmentItem
     * 
     * @param string $trackingNumber
     * 
     * @return Shipment
     */
    public function cancel(string $trackingNumber): ?Shipment
    {
        $response = $this->sdk->getHttpClient()->patch('/shipments/' . $trackingNumber . '/cancel', [
            'Content-Type' => 'application/merge-patch+json',
        ]);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Shipment($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/shipments_packingShipmentItem
     * 
     * @param string $trackingNumber
     * 
     * @return array
     */
    public function packing(string $trackingNumber): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/shipments/' . $trackingNumber . '/packing');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/getShipmentCollection
     * 
     * @param int $page
     * 
     * @return array
     */
    public function all(int $page = 1): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/shipments?page=' . $page);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $shipments = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $shipments[] = new Shipment($item);
                }
            }
            return $shipments;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/shipments_signatureShipmentItem
     * 
     * @param string $request
     * 
     * @return string
     */
    public function signature(string $request): string
    {
        $body = json_encode(['request' => $request]);

        $response = $this->sdk->getHttpClient()->post('/signature', [], $body);

        if ($response->getStatusCode() == 200) {            
            return $response->getBody()->getContents();
        } else {
            return $this->throwException($result, self::TYPE_STRING, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Shipment/operation/shipments_certificateShipmentItem
     * 
     * @return string
     */
    public function certificate(): string
    {
        $response = $this->sdk->getHttpClient()->post('/certificate');

        if ($response->getStatusCode() == 200) {            
            return $response->getBody()->getContents();
        } else {
            return $this->throwException($result, self::TYPE_STRING, $response->getStatusCode());
        }
    }
}