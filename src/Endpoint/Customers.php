<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint;

use WorldOptions\Utils\Response;
use WorldOptions\Model\Customer;
use WorldOptions\Endpoint\Endpoint;

final class Customers extends Endpoint
{

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Customer/operation/customer_get_infoCustomerItem
     * 
     * @return Customer
     */
    public function info(): ?Customer
    {
        $response = $this->sdk->getHttpClient()->get('/customers/info');
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Customer($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Customer/operation/customer_authorizeCustomerItem
     * 
     * @return bool
     */
    public function authorize(Customer $customer): bool
    {
        $body = json_encode([
            'username' => $customer->getUsername(),
            'password' => $customer->getPassword(),
            'meternumber' => $customer->getMeternumber(),
            'country' => $customer->getCountry()->getCode(),
        ]);
        $response = $this->sdk->getHttpClient()->post('/customers/authorize', [], $body);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            return $result['success'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Customer/operation/customer_contactCustomerItem
     * 
     * @param string $type
     * @param string $company
     * @param string $firstname
     * @param string $lastname
     * @param string $message
     * @param string $phone
     * @param string $email
     * @param string $country
     * 
     * @return bool
     */
    public function contact(string $type, string $company, string $firstname, string $lastname, string $message, string $phone, string $email, string $country): bool
    {
        $body = [
            'type' => $type,
            'company' => $company,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'message' => $message,
            'phone' => $phone,
            'email' => $email,
            'country' => $country,
        ];
        
        $response = $this->sdk->getHttpClient()->post('/customers/contact', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return $result['result'];
        } else {
            return $this->throwException($result, self::TYPE_BOOL, $response->getStatusCode());
        }
    }
}