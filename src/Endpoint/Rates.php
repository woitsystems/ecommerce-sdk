<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Endpoint;

use WorldOptions\Utils\Response;
use WorldOptions\WorldOptions as Sdk;
use WorldOptions\Endpoint\Rate\Boxes;
use WorldOptions\Endpoint\Rate\Carriers;
use WorldOptions\Endpoint\Rate\CustomRates;
use WorldOptions\Endpoint\Rate\BackupRates;
use WorldOptions\Endpoint\Rate\ExternalRates;
use WorldOptions\Endpoint\Rate\InternalRates;
use WorldOptions\Endpoint\Rate\Items;
use WorldOptions\Model\Rate;
use WorldOptions\Model\RateRequest;
use WorldOptions\Model\Rate\PackingRequest;
use WorldOptions\Model\Rate\RatesRequest;

final class Rates extends Endpoint
{
    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Rate/operation/getRateCollection
     * 
     * @param RateRequest $rate
     * 
     * @return array
     */
    public function create(RateRequest $rate): ?Rate
    {
        $body = [
            'destination' => [
                'name' => $rate->getDestination()->getName(),
                'company' => $rate->getDestination()->getCompany(),
                'firstname' => $rate->getDestination()->getFirstname(),
                'lastname' => $rate->getDestination()->getLastname(),
                'street' => $rate->getDestination()->getStreet(),
                'postcode' => $rate->getDestination()->getPostcode(),
                'city' => $rate->getDestination()->getCity(),
                'state' => $rate->getDestination()->getState(),
                'province' => $rate->getDestination()->getProvince() ? $rate->getDestination()->getProvince()->getId() : null,
                'country' => $rate->getDestination()->getCountry()->getCode(),
                'phone' => $rate->getDestination()->getPhone(),
                'email' => $rate->getDestination()->getEmail(),
            ],
            'frontend' => $rate->isFrontend(),
            'residental' => $rate->isResidental(),
            'currency' => $rate->getCurrency()->getCode(),
            'items' => [],
        ];

        if ($rate->isPacking() !== null) {
            $body['packing'] = $rate->isPacking();
        }

        if ($rate->getOrder()) {
            $body['order'] = $rate->getOrder();
        }

        if ($rate->getItems()) {
            foreach ($rate->getItems() as $i => $product) {
                $dimensions = [];
                if ($product->getDimensions()) {
                    foreach ($product->getDimensions() as $dims) {
                        $dimensions[] = $dims->__toArray();
                    }
                }
                $body['items'][$i] = [
                    'name' => $product->getName(),
                    'reference' => $product->getReference(),
                    'productId' => $product->getProductId(),
                    'quantity' => $product->getQuantity(),
                    'price' => $product->getPrice(),
                    'dimensions' => $dimensions,
                ];

                if ($product->getCountry()) {
                    $body['items'][$i]['country'] = $product->getCountry()->getCode();
                }

                if ($product->getType()) {
                    $body['items'][$i]['type'] = $product->getType();
                }

                if ($product->getVariantId()) {
                    $body['items'][$i]['variantId'] = $product->getVariantId();
                }
            }
        }

        if ($rate->getBoxes()) {
            foreach ($rate->getBoxes() as $i => $box) {
                $body['boxes'][$i] = [
                    'width' => $box->getWidth(),
                    'length' => $box->getLength(),
                    'depth' => $box->getDepth(),
                    'weight' => $box->getWeight(),
                ];
            }
        }

        if ($rate->getOrigin()) {
            $body['origin'] = [
                'name' => $rate->getOrigin()->getName(),
                'company' => $rate->getOrigin()->getCompany(),
                'firstname' => $rate->getOrigin()->getFirstname(),
                'lastname' => $rate->getOrigin()->getLastname(),
                'street' => $rate->getOrigin()->getStreet(),
                'postcode' => $rate->getOrigin()->getPostcode(),
                'city' => $rate->getOrigin()->getCity(),
                'state' => $rate->getOrigin()->getState(),
                'province' => $rate->getOrigin()->getProvince() ? $rate->getOrigin()->getProvince()->getId() : '',
                'country' => $rate->getOrigin()->getCountry()->getCode(),
                'phone' => $rate->getOrigin()->getPhone(),
                'email' => $rate->getOrigin()->getEmail(),
            ];
        }

        $response = $this->sdk->getHttpClient()->post('/rates', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return new Rate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Rate/operation/multi_ratesRateCollection
     * 
     * @param RatesRequest $rates
     * 
     * @return array
     */
    public function multi(RatesRequest $rates): ?array
    {
        $body = [
            'residental' => $rates->isResidental(),
            'frontend' => $rates->isFrontend(),
            'currency' => $rates->getCurrency()->getCode(),
            'data' => [],
        ];

        if ($rates->isPacking() !== null) {
            $body['packing'] = $rates->isPacking();
        }

        if ($rates->getOrder()) {
            $body['order'] = $rates->getOrder();
        }

        foreach ($rates->getData() as $i => $rate) {
            $body['data'][$i]['destination'] = [
                'name' => $rate->getDestination()->getName(),
                'company' => $rate->getDestination()->getCompany(),
                'firstname' => $rate->getDestination()->getFirstname(),
                'lastname' => $rate->getDestination()->getLastname(),
                'street' => $rate->getDestination()->getStreet(),
                'postcode' => $rate->getDestination()->getPostcode(),
                'city' => $rate->getDestination()->getCity(),
                'state' => $rate->getDestination()->getState(),
                'province' => $rate->getDestination()->getProvince() ? $rate->getDestination()->getProvince()->getId() : null,
                'country' => $rate->getDestination()->getCountry()->getCode(),
                'phone' => $rate->getDestination()->getPhone(),
                'email' => $rate->getDestination()->getEmail(),
            ];

            if ($rate->getItems()) {
                foreach ($rate->getItems() as $j => $product) {
                    $dimensions = [];
                    if ($product->getDimensions()) {
                        foreach ($product->getDimensions() as $dims) {
                            $dimensions[] = $dims->__toArray();
                        }
                    }
                    $body['data'][$i]['items'][$j] = [
                        'name' => $product->getName(),
                        'reference' => $product->getReference(),
                        'productId' => $product->getProductId(),
                        'quantity' => $product->getQuantity(),
                        'price' => $product->getPrice(),
                        'dimensions' => $dimensions,
                    ];
    
                    if ($product->getCountry()) {
                        $body['data'][$i]['items'][$j]['country'] = $product->getCountry()->getCode();
                    }
    
                    if ($product->getType()) {
                        $body['data'][$i]['items'][$j]['type'] = $product->getType();
                    }
    
                    if ($product->getVariantId()) {
                        $body['data'][$i]['items'][$j]['variantId'] = $product->getVariantId();
                    }
                }
            }

            if ($rate->getBoxes()) {
                foreach ($rate->getBoxes() as $j => $box) {
                    $body['data'][$i]['boxes'][$j] = [
                        'width' => $box->getWidth(),
                        'length' => $box->getLength(),
                        'depth' => $box->getDepth(),
                        'weight' => $box->getWeight(),
                    ];
                }
            }
    
            if ($rate->getOrigin()) {
                $body['data'][$i]['origin'] = [
                    'name' => $rate->getOrigin()->getName(),
                    'company' => $rate->getOrigin()->getCompany(),
                    'firstname' => $rate->getOrigin()->getFirstname(),
                    'lastname' => $rate->getOrigin()->getLastname(),
                    'street' => $rate->getOrigin()->getStreet(),
                    'postcode' => $rate->getOrigin()->getPostcode(),
                    'city' => $rate->getOrigin()->getCity(),
                    'state' => $rate->getOrigin()->getState(),
                    'province' => $rate->getOrigin()->getProvince() ? $rate->getOrigin()->getProvince()->getId() : '',
                    'country' => $rate->getOrigin()->getCountry()->getCode(),
                    'phone' => $rate->getOrigin()->getPhone(),
                    'email' => $rate->getOrigin()->getEmail(),
                ];
            }
        }

        $response = $this->sdk->getHttpClient()->post('/rates/multi', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            $rates = [];
            foreach ($result as $item) {
                $rates[] = new Rate($item);
            }
            return $rates;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * 
     * @param PackingRequest $packing
     * 
     * @return array
     */
    public function packing(PackingRequest $packing): ?array
    {
        $body = [
            'currency' => $packing->getCurrency()->getCode(),
            'data' => [],
        ];

        foreach ($packing->getData() as $i => $item) {

            if ($item->getItems()) {
                foreach ($item->getItems() as $j => $product) {
                    $dimensions = [];
                    if ($product->getDimensions()) {
                        foreach ($product->getDimensions() as $dims) {
                            $dimensions[] = $dims->__toArray();
                        }
                    }
                    $body['data'][$i]['items'][$j] = [
                        'name' => $product->getName(),
                        'reference' => $product->getReference(),
                        'productId' => $product->getProductId(),
                        'quantity' => $product->getQuantity(),
                        'price' => $product->getPrice(),
                        'dimensions' => $dimensions,
                    ];
    
                    if ($product->getCountry()) {
                        $body['data'][$i]['items'][$j]['country'] = $product->getCountry()->getCode();
                    }
    
                    if ($product->getType()) {
                        $body['data'][$i]['items'][$j]['type'] = $product->getType();
                    }
    
                    if ($product->getVariantId()) {
                        $body['data'][$i]['items'][$j]['variantId'] = $product->getVariantId();
                    }
                }
            }

            if ($item->getBoxes()) {
                foreach ($item->getBoxes() as $j => $box) {
                    $body['data'][$i]['boxes'][$j] = [
                        'width' => $box->getWidth(),
                        'length' => $box->getLength(),
                        'depth' => $box->getDepth(),
                        'weight' => $box->getWeight(),
                    ];
                }
            }
        }

        $response = $this->sdk->getHttpClient()->post('/rates/packing', [], json_encode($body));
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {  
            return $result;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Rate/operation/getRateItem
     * 
     * @param int $id
     * 
     * @return Rate
     */
    public function get(int $id): ?Rate
    {
        $response = $this->sdk->getHttpClient()->get('/rates/' . $id);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {            
            return new Rate($result);
        } else {
            return $this->throwException($result, self::TYPE_CLASS, $response->getStatusCode());
        }
    }

    /**
     * @tutorial https://ecommerce.worldoptions.com/api/docs?ui=re_doc#tag/Rate/operation/getRateCollection
     * 
     * @param int $page
     * 
     * @return array
     */
    public function all(int $page = 1): ?array
    {
        $response = $this->sdk->getHttpClient()->get('/rates?page=' . $page);
        $result = Response::getContent($response);
        if ($response->getStatusCode() == 200) {
            $rates = [];
            if (count($result)) {
                foreach ($result as $item) {
                    $rates[] = new Rate($item);
                }
            }
            return $rates;
        } else {
            return $this->throwException($result, self::TYPE_ARRAY, $response->getStatusCode());
        }
    }

    public function boxes(): Boxes
    {
        return new Boxes($this->sdk);
    }

    public function carriers(): Carriers
    {
        return new Carriers($this->sdk);
    }

    public function customRates(): CustomRates
    {
        return new CustomRates($this->sdk);
    }

    public function backupRates(): BackupRates
    {
        return new BackupRates($this->sdk);
    }

    public function externalRates(): ExternalRates
    {
        return new ExternalRates($this->sdk);
    }

    public function internalRates(): InternalRates
    {
        return new InternalRates($this->sdk);
    }

    public function items(): Items
    {
        return new Items($this->sdk);
    }
}