<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Carrier\Service as CarrierService;
use WorldOptions\Model\Carrier;

final class Order extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var array $shipments
     */
    protected array $shipments;

    /**
     * @var string $reference
     */
    protected string $reference;

    /**
     * @var string $orderId
     */
    protected string $orderId;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var array $data
     */
    protected array $data;

    /**
     * @var array $rates
     */
    protected array $rates;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setReference($data['reference']);
            $this->setOrderId($data['orderId']);
            $this->setAmount($data['amount']);
            if (isset($data['rates']) && $data['rates']) {
                foreach ($data['rates'] as $rateItem) {
                    $rate = new Rate($rateItem);
                    $this->addRate($rate);
                }
            }
            
            if (isset($data['data']) && $data['data']) {
                $this->setData($data['data']);
            }

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['shipments']) && $data['shipments']) {
                foreach ($data['shipments'] as $shipmentItem) {
                    $shipment = new Shipment($shipmentItem);
                    
                    $this->addShipment($shipment);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getShipments(): array
    {
        return isset($this->shipments) ? $this->shipments : [];
    }

    /**
     *
     * @param Shipment $shipment
     * @return self
     */
    public function addShipment(Shipment $shipment): self
    {
        $this->shipments[] = $shipment;
        return $this;
    }

    /**
     *
     * @param array $shipments
     * @return self
     */
    public function setShipments(array $shipments): self
    {
        $this->shipments = $shipments;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReference(): ?string
    {
        return $this->reference;
    }

    /**
     *
     * @param string $reference
     * @return self
     */
    public function setReference(?string $reference): self
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getOrderId(): string
    {
        return $this->orderId;
    }

    /**
     *
     * @param string $orderId
     * @return self
     */
    public function setOrderId(string $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getRates(): array
    {
        return isset($this->rates) ? $this->rates : [];
    }

    /**
     *
     * @param Rate $rate
     * @return self
     */
    public function addRate(Rate $rate): self
    {
        $this->rates[] = $rate;
        return $this;
    }

    /**
     *
     * @param array $rates
     * @return self
     */
    public function setRates(array $rates): self
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getData(): array
    {
        return isset($this->data) ? $this->data : [];
    }

    /**
     *
     * @param array $data
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }
}