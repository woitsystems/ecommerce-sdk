<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;
use WorldOptions\Model\Localization\Country;

final class ProductDimension extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var float $width
     */
    protected float $width;

    /**
     * @var float $length
     */
    protected float $length;

    /**
     * @var float $depth
     */
    protected float $depth;

    /**
     * @var float $weight
     */
    protected float $weight;

    /**
     * @var bool $backend
     */
    protected bool $backend = false;

    /**
     * @var Product $product
     */
    protected Product $product;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setWidth($data['width']);
            $this->setLength($data['length']);
            $this->setDepth($data['depth']);
            $this->setWeight($data['weight']);
            $this->setBackend($data['backend']);

            if (isset($data['product']) && $data['product']) {
                $product = new Product($data['product']);
                $this->setProduct($product);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     *
     * @param float $width
     * @return self
     */
    public function setWidth(float $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     *
     * @param float $length
     * @return self
     */
    public function setLength(float $length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDepth(): float
    {
        return $this->depth;
    }

    /**
     *
     * @param float $depth
     * @return self
     */
    public function setDepth(float $depth): self
    {
        $this->depth = $depth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     *
     * @param float $weight
     * @return self
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isBackend(): bool
    {
        return $this->backend;
    }

    /**
     *
     * @param bool $backend
     * @return self
     */
    public function setBackend(bool $backend): self
    {
        $this->backend = $backend;
        return $this;
    }

    /**
     *
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     *
     * @param Product $product
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }
}