<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;

final class CreateShipment extends Model
{
    /**
     * @var int $rate
     */
    protected int $rate;

    /**
     * @var int $order
     */
    protected int $order;

    /**
     * @var int $carrier
     */
    protected int $carrier;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setRate($data['rate']);
            $this->setOrder($data['order']);
            $this->setCarrier($data['carrier']);
        }
    }

    /**
     *
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     *
     * @param int $rate
     * @return self
     */
    public function setRate(int $rate): self
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     *
     * @param int $order
     * @return self
     */
    public function setOrder(int $order): self
    {
        $this->order = $order;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getCarrier(): int
    {
        return $this->carrier;
    }

    /**
     *
     * @param int $carrier
     * @return self
     */
    public function setCarrier(int $carrier): self
    {
        $this->carrier = $carrier;
        return $this;
    }
}