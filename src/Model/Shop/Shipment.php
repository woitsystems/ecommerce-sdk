<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;
use WorldOptions\Model\Carrier\Service as CarrierService;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Rate;

final class Shipment extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var Order $order
     */
    protected Order $order;

    /**
     * @var CarrierService $carrierService
     */
    protected CarrierService $carrierService;

    /**
     * @var ?string $trackingNumber
     */
    protected ?string $trackingNumber;

    /**
     * @var float $shippingAmount
     */
    protected float $shippingAmount;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @var array $shippingLabels
     */
    protected array $shippingLabels = [];

    /**
     * @var bool $canceled
     */
    protected bool $canceled = false;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            if (isset($data['trackingNumber'])) {
                $this->setTrackingNumber($data['trackingNumber']);
            }
            $this->setShippingAmount($data['shippingAmount']);
            $this->setShippingLabels($data['shippingLabels']);
            $this->setCanceled($data['canceled']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['carrierService']) && $data['carrierService']) {
                $carrierService = new CarrierService($data['carrierService']);
                $this->setCarrierService($carrierService);
            }

            if (isset($data['rate']) && $data['rate']) {
                $rate = new Rate($data['rate']);
                $this->setRate($rate);
            }

            if (isset($data['order']) && $data['order']) {
                $order = new Order($data['order']);
                $this->setOrder($order);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     *
     * @param Order $order
     * @return self
     */
    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }

    /**
     *
     * @return CarrierService
     */
    public function getCarrierService(): CarrierService
    {
        return $this->carrierService;
    }

    /**
     *
     * @param CarrierService $carrierService
     * @return self
     */
    public function setCarrierService(CarrierService $carrierService): self
    {
        $this->carrierService = $carrierService;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTrackingNumber(): ?string
    {
        return isset($this->trackingNumber) ? $this->trackingNumber : null;
    }

    /**
     *
     * @param string $trackingNumber
     * @return self
     */
    public function setTrackingNumber(?string $trackingNumber): self
    {
        $this->trackingNumber = $trackingNumber;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getShippingAmount(): float
    {
        return $this->shippingAmount;
    }

    /**
     *
     * @param float $shippingAmount
     * @return self
     */
    public function setShippingAmount(float $shippingAmount): self
    {
        $this->shippingAmount = $shippingAmount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return Rate
     */
    public function getRate(): Rate
    {
        return $this->rate;
    }

    /**
     *
     * @param Rate $rate
     * @return self
     */
    public function setRate(Rate $rate): self
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getShippingLabels(): array
    {
        return $this->shippingLabels;
    }

    /**
     *
     * @param array $shippingLabels
     * @return self
     */
    public function setShippingLabels(array $shippingLabels): self
    {
        $this->shippingLabels = $shippingLabels;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isCanceled(): bool
    {
        return $this->canceled;
    }

    /**
     *
     * @param bool $canceled
     * @return self
     */
    public function setCanceled(bool $canceled): self
    {
        $this->canceled = $canceled;
        return $this;
    }
}