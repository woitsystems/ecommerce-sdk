<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;
use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Currency;

final class Product extends Model
{
    public const TYPE_INDIVIDUAL = 'individual';
    public const TYPE_SHARED = 'shared';

    public const TYPES = [
        self::TYPE_INDIVIDUAL,
        self::TYPE_SHARED,
    ];

    public const SHIPPING_FEE_TYPE_AMOUNT = 'amount';
    public const SHIPPING_FEE_TYPE_PERCENTAGE = 'percentage';

    public const SHIPPING_FEE_TYPES = [
        self::SHIPPING_FEE_TYPE_AMOUNT,
        self::SHIPPING_FEE_TYPE_PERCENTAGE,
    ];
    
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $description
     */
    protected ?string $description;

    /**
     * @var string $htsNumber
     */
    protected ?string $htsNumber;

    /**
     * @var string $reference
     */
    protected ?string $reference;

    /**
     * @var string $productId
     */
    protected string $productId;

    /**
     * @var string $variantId
     */
    protected ?string $variantId;

    /**
     * @var string $type
     */
    protected string $type;

    /**
     * @var bool $keepFlat
     */
    protected bool $keepFlat = false;

    /**
     * @var bool $enablePacking
     */
    protected bool $enablePacking = true;

    /**
     * @var Country $country
     */
    protected Country $country;

    /**
     * @var int $shippingDays
     */
    protected $shippingDays;

    /**
     * @var float $shippingFeeAmount
     */
    protected $shippingFeeAmount;

    /**
     * @var string $shippingFeeType
     */
    protected $shippingFeeType;

    /**
     * @var float $price
     */
    protected float $price;

    /**
     * @var int $quantity
     */
    protected ?int $quantity;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @var array $dimensions
     */
    protected array $dimensions = [];

    /**
     * @var array $boxes
     */
    protected array $boxes;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            if (isset($data['description'])) {
                $this->setDescription($data['description']);
            }
            if (isset($data['htsNumber'])) {
                $this->setHtsNumber($data['htsNumber']);
            }
            if (isset($data['reference'])) {
                $this->setReference($data['reference']);
            }
            $this->setProductId($data['productId']);
            if (isset($data['variantId'])) {
                $this->setVariantId($data['variantId']);
            }
            $this->setType($data['type']);
            $this->setKeepFlat($data['keepFlat']);
            $this->setEnablePacking($data['enablePacking']);

            if (isset($data['shippingDays'])) {
                $this->setShippingDays($data['shippingDays']);
            }

            if (isset($data['shippingFeeAmount'])) {
                $this->setShippingFeeAmount($data['shippingFeeAmount']);
            }

            if (isset($data['shippingFeeType'])) {
                $this->setShippingFeeType($data['shippingFeeType']);
            }

            if (isset($data['price'])) {
                $this->setPrice($data['price']);
            }

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['country']) && $data['country']) {
                $country = new Country($data['country']);
                $this->setCountry($country);
            }
            
            if (isset($data['dimensions']) && $data['dimensions']) {
                foreach ($data['dimensions'] as $item) {
                    $dimensions = new ProductDimension($item);
                    $this->addDimension($dimensions);
                }
            }

            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return isset($this->id) ? $this->id : null;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): ?string
    {
        return isset($this->name) ? $this->name : null;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDescription(): ?string
    {
        return isset($this->description) ? $this->description : null;
    }

    /**
     *
     * @param string $description
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getHtsNumber(): ?string
    {
        return isset($this->htsNumber) ? $this->htsNumber : null;
    }

    /**
     *
     * @param string $htsNumber
     * @return self
     */
    public function setHtsNumber(?string $htsNumber): self
    {
        $this->htsNumber = $htsNumber;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getReference(): ?string
    {
        return isset($this->reference) ? $this->reference : null;
    }

    /**
     *
     * @param string $reference
     * @return self
     */
    public function setReference(?string $reference): self
    {
        $this->reference = $reference;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getProductId(): ?string
    {
        return isset($this->productId) ? $this->productId : null;
    }

    /**
     *
     * @param string $productId
     * @return self
     */
    public function setProductId(string $productId): self
    {
        $this->productId = $productId;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getVariantId(): ?string
    {
        return isset($this->variantId) ? $this->variantId : null;
    }

    /**
     *
     * @param string $variantId
     * @return self
     */
    public function setVariantId(?string $variantId): self
    {
        $this->variantId = $variantId;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getType(): ?string
    {
        return isset($this->type) ? $this->type : null;
    }

    /**
     *
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isKeepFlat(): ?bool
    {
        return isset($this->keepFlat) ? $this->keepFlat : null;
    }

    /**
     *
     * @param bool $keepFlat
     * @return self
     */
    public function setKeepFlat(bool $keepFlat): self
    {
        $this->keepFlat = $keepFlat;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnablePacking(): bool
    {
        return $this->enablePacking;
    }

    /**
     *
     * @param bool $enablePacking
     * @return self
     */
    public function setEnablePacking(bool $enablePacking): self
    {
        $this->enablePacking = $enablePacking;
        return $this;
    }

    /**
     *
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return isset($this->country) ? $this->country : null;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getShippingDays(): ?int
    {
        return isset($this->shippingDays) ? $this->shippingDays : null;
    }

    /**
     *
     * @param int $shippingDays
     * @return self
     */
    public function setShippingDays(int $shippingDays): self
    {
        $this->shippingDays = $shippingDays;
        return $this;
    }

    /**
     *
     * @return float|null
     */
    public function getShippingFeeAmount(): ?float
    {
        return isset($this->shippingFeeAmount) ? $this->shippingFeeAmount : null;
    }

    /**
     *
     * @param float $shippingFeeAmount
     * @return self
     */
    public function setShippingFeeAmount(float $shippingFeeAmount): self
    {
        $this->shippingFeeAmount = $shippingFeeAmount;
        return $this;
    }

    /**
     *
     * @param string $shippingFeeType
     * @return self
     */
    public function setShippingFeeType(string $shippingFeeType): self
    {
        if ($shippingFeeType) {
            if (in_array($shippingFeeType, self::SHIPPING_FEE_TYPES)) {
                $this->shippingFeeType = $shippingFeeType;
            } else {
                $this->shippingFeeType = self::SHIPPING_FEE_TYPE_AMOUNT;
            }
        } else {
            $this->shippingFeeType = null;
        }
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getShippingFeeType(): ?string
    {
        return isset($this->shippingFeeType) ? $this->shippingFeeType : null;
    }

    /**
     *
     * @return float
     */
    public function getPrice(): ?float
    {
        return isset($this->price) ? $this->price : null;
    }

    /**
     *
     * @param float $price
     * @return self
     */
    public function setPrice(?float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getQuantity(): ?int
    {
        return isset($this->quantity) ? $this->quantity : null;
    }

    /**
     *
     * @param int $quantity
     * @return self
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): ?Currency
    {
        return isset($this->currency) ? $this->currency : null;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getDimensions(): ?array
    {
        return isset($this->dimensions) ? $this->dimensions : null;
    }

    /**
     *
     * @param ProductDimension $dimension
     * @return self
     */
    public function addDimension(ProductDimension $dimension): self
    {
        $this->dimensions[] = $dimension;
        return $this;
    }

    /**
     *
     * @param array $dimensions
     * @return self
     */
    public function setDimensions(array $dimensions): self
    {
        $this->dimensions = $dimensions;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getBoxes(): ?array
    {
        return isset($this->boxes) ? $this->boxes : null;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }
}