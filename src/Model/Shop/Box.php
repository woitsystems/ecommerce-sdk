<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;

final class Box extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var float $outerWidth
     */
    protected float $outerWidth;

    /**
     * @var float $outerLength
     */
    protected float $outerLength;

    /**
     * @var float $outerDepth
     */
    protected float $outerDepth;

    /**
     * @var float $innerWidth
     */
    protected float $innerWidth;

    /**
     * @var float $innerLength
     */
    protected float $innerLength;

    /**
     * @var float $innerDepth
     */
    protected float $innerDepth;

    /**
     * @var float $emptyWeight
     */
    protected float $emptyWeight;

    /**
     * @var float $maxWeight
     */
    protected float $maxWeight;

    /**
     * @var bool $active
     */
    protected bool $active;

    /**
     * @var bool $deleted
     */
    protected bool $deleted;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            $this->setOuterWidth($data['outerWidth']);
            $this->setOuterLength($data['outerLength']);
            $this->setOuterDepth($data['outerDepth']);
            $this->setInnerWidth($data['innerWidth']);
            $this->setInnerLength($data['innerLength']);
            $this->setInnerDepth($data['innerDepth']);
            $this->setEmptyWeight($data['emptyWeight']);
            $this->setMaxWeight($data['maxWeight']);
            $this->setActive($data['active']);
            $this->setDeleted($data['deleted']);
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getOuterWidth(): float
    {
        return $this->outerWidth;
    }

    /**
     *
     * @param float $outerWidth
     * @return self
     */
    public function setOuterWidth(float $outerWidth): self
    {
        $this->outerWidth = $outerWidth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getOuterLength(): float
    {
        return $this->outerLength;
    }

    /**
     *
     * @param float $outerLength
     * @return self
     */
    public function setOuterLength(float $outerLength): self
    {
        $this->outerLength = $outerLength;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getOuterDepth(): float
    {
        return $this->outerDepth;
    }

    /**
     *
     * @param float $outerDepth
     * @return self
     */
    public function setOuterDepth(float $outerDepth): self
    {
        $this->outerDepth = $outerDepth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getInnerWidth(): float
    {
        return $this->innerWidth;
    }

    /**
     *
     * @param float $innerWidth
     * @return self
     */
    public function setInnerWidth(float $innerWidth): self
    {
        $this->innerWidth = $innerWidth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getInnerLength(): float
    {
        return $this->innerLength;
    }

    /**
     *
     * @param float $innerLength
     * @return self
     */
    public function setInnerLength(float $innerLength): self
    {
        $this->innerLength = $innerLength;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getInnerDepth(): float
    {
        return $this->innerDepth;
    }

    /**
     *
     * @param float $innerDepth
     * @return self
     */
    public function setInnerDepth(float $innerDepth): self
    {
        $this->innerDepth = $innerDepth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getEmptyWeight(): float
    {
        return $this->emptyWeight;
    }

    /**
     *
     * @param float $emptyWeight
     * @return self
     */
    public function setEmptyWeight(float $emptyWeight): self
    {
        $this->emptyWeight = $emptyWeight;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getMaxWeight(): float
    {
        return $this->maxWeight;
    }

    /**
     *
     * @param float $maxWeight
     * @return self
     */
    public function setMaxWeight(float $maxWeight): self
    {
        $this->maxWeight = $maxWeight;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     *
     * @param bool $active
     * @return self
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     *
     * @param bool $deleted
     * @return self
     */
    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;
        return $this;
    }
}