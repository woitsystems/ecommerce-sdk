<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Province;
use WorldOptions\Model\Model;

final class Address  extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $company
     */
    protected string $company;

    /**
     * @var string $firstName
     */
    protected string $firstName;

    /**
     * @var string $lastName
     */
    protected string $lastName;

    /**
     * @var string $street
     */
    protected string $street;

    /**
     * @var string $postcode
     */
    protected string $postcode;

    /**
     * @var string $city
     */
    protected string $city;

    /**
     * @var string $state
     */
    protected string $state;

    /**
     * @var Province $province
     */
    protected Province $province;

    /**
     * @var Country $country
     */
    protected Country $country;

    /**
     * @var string $phone
     */
    protected string $phone;

    /**
     * @var string $email
     */
    protected string $email;

    /**
     * @var string $hash
     */
    protected string $hash;

    /**
     * @var bool $hash
     */
    protected string $own;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            if (isset($data['company']) && $data['company']) {
                $this->setCompany($data['company']);
            }
            if (isset($data['firstName']) && $data['firstName']) {
                $this->setFirstName($data['firstName']);
            }
            if (isset($data['lastName']) && $data['lastName']) {
                $this->setLastName($data['lastName']);
            }
            $this->setStreet($data['street']);
            $this->setPostcode($data['postcode']);
            $this->setCity($data['city']);
            if (isset($data['state']) && $data['state']) {
                $this->setState($data['state']);
            }
            if (isset($data['phone']) && $data['phone']) {
                $this->setPhone($data['phone']);
            }
            if (isset($data['email']) && $data['email']) {
                $this->setEmail($data['email']);
            }
            if (isset($data['hash']) && $data['hash']) {
                $this->setHash($data['hash']);
            }
            if (isset($data['province']) && $data['province']) {
                $province = new Province($data['province']);
                $this->setProvince($province);
            }

            if (isset($data['country']) && $data['country']) {
                $country = new Country($data['country']);
                $this->setCountry($country);
            }

            $this->setOwn(isset($data['own']) && $data['own'] ? true : false);
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCompany(): string
    {
        return isset($this->company) ? $this->company : '';
    }

    /**
     *
     * @param string $company
     * @return self
     */
    public function setCompany(string $company): self
    {
        $this->company = $company;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return isset($this->firstName) ? $this->firstName : ''; 
    }

    /**
     *
     * @param string $firstName
     * @return self
     */
    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLastName(): string
    {
        return isset($this->lastName) ? $this->lastName : '';
    }

    /**
     *
     * @param string $lastName
     * @return self
     */
    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     *
     * @param string $street
     * @return self
     */
    public function setStreet(string $street): self
    {
        $this->street = $street;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->postcode;
    }

    /**
     *
     * @param string $postcode
     * @return self
     */
    public function setPostcode(string $postcode): self
    {
        $this->postcode = $postcode;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     *
     * @param string $city
     * @return self
     */
    public function setCity(string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getState(): string
    {
        return isset($this->state) ? $this->state : '';
    }

    /**
     *
     * @param string $state
     * @return self
     */
    public function setState(string $state): self
    {
        $this->state = $state;
        return $this;
    }

    /**
     *
     * @return Province|null
     */
    public function getProvince(): ?Province
    {
        return isset($this->province) ? $this->province : null;
    }

    /**
     *
     * @param Province $province
     * @return self
     */
    public function setProvince(Province $province): self
    {
        $this->province = $province;
        return $this;
    }

    /**
     *
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPhone(): string
    {
        return isset($this->phone) ? $this->phone : '';
    }

    /**
     *
     * @param string $phone
     * @return self
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail(): string
    {
        return isset($this->email) ? $this->email : '';
    }

    /**
     *
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getHash(): ?string
    {
        return isset($this->hash) ? $this->hash : null;
    }

    /**
     *
     * @param string $hash
     * @return self
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     *
     * @return bool|null
     */
    public function isOwn(): ?bool
    {
        return isset($this->own) ? $this->own : false;
    }

    /**
     *
     * @param bool $own
     * @return self
     */
    public function setOwn(bool $own): self
    {
        $this->own = $own;
        return $this;
    }
}