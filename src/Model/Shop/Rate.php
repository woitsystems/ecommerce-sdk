<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Model;

final class Rate extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var ?int $service
     */
    protected ?int $service;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setService($data['service']);
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getService(): ?int
    {
        return isset($this->service) ? $this->service : null;
    }

    /**
     *
     * @param int $service
     * @return self
     */
    public function setService(?int $service): self
    {
        $this->service = $service;
        return $this;
    }
}