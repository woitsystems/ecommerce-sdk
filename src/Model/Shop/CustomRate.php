<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Shop;

use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Localization\Zone;
use WorldOptions\Model\Model;

final class CustomRate extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var float $currency
     */
    protected Currency $currency;

    /**
     * @var array $countries
     */
    protected array $countries;

    /**
     * @var array $zones
     */
    protected array $zones;

    /**
     * @param ?array $data
     *
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            $this->setAmount($data['amount']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['countries']) && $data['countries']) {
                foreach ($data['countries'] as $item) {
                    $country = new Country($item);
                    $this->addCountry($country);
                }
            }

            if (isset($data['zones']) && $data['zones']) {
                foreach ($data['zones'] as $item) {
                    $zone = new Zone($item);
                    $carrier->addZone($zone);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getCountries(): ?array
    {
        return isset($this->countries) ? $this->countries : null;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function addCountry(Country $country): self
    {
        $this->countries[] = $country;
        return $this;
    }

    /**
     *
     * @param array $countries
     * @return self
     */
    public function setCountries(array $countries): self
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getZones(): ?array
    {
        return isset($this->zones) ? $this->zones : null;
    }

    /**
     *
     * @param Zone $zone
     * @return self
     */
    public function addZone(Zone $zone): self
    {
        $this->zones[] = $zone;
        return $this;
    }

    /**
     *
     * @param array $zones
     * @return self
     */
    public function setZones(array $zones): self
    {
        $this->zones = $zones;
        return $this;
    }
}
