<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Localization\Zone;
use WorldOptions\Model\Shop\Box;
use WorldOptions\Model\Carrier\Service;

final class Carrier extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $code
     */
    protected string $code;

    /**
     * @var bool $external
     */
    protected bool $external = true;

    /**
     * @var array $services
     */
    protected array $services = [];

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     * @var array $countries
     */
    protected array $countries = [];

    /**
     * @var array $zones
     */
    protected array $zones = [];

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            if (isset($data['code']) && $data['code']) {
                $this->setCode($data['code']);
            }
            $this->setExternal($data['external']);

            if (isset($data['services']) && $data['services']) {
                foreach ($data['services'] as $item) {
                    $service = new Service($item);
                    $this->addService($service);
                }
            }

            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }

            if (isset($data['countries']) && $data['countries']) {
                foreach ($data['countries'] as $item) {
                    $country = new Country($item);
                    $this->addCountry($country);
                }
            }

            if (isset($data['zones']) && $data['zones']) {
                foreach ($data['zones'] as $item) {
                    $zone = new Zone($item);
                    $carrier->addZone($zone);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     *
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }
    
    /**
     *
     * @return bool
     */
    public function isExternal(): bool
    {
        return $this->external;
    }

    /**
     *
     * @param string $external
     * @return self
     */
    public function setExternal(?bool $external): self
    {
        $this->external = $external;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }

    /**
     *
     * @param Service $service
     * @return self
     */
    public function addService(Service $service): self
    {
        $this->services[] = $service;
        return $this;
    }

    /**
     *
     * @param array $services
     * @return self
     */
    public function setServices(array $services): self
    {
        $this->services = $services;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function addCountry(Country $country): self
    {
        $this->countries[] = $country;
        return $this;
    }

    /**
     *
     * @param array $countries
     * @return self
     */
    public function setCountries(array $countries): self
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getZones(): array
    {
        return $this->zones;
    }

    /**
     *
     * @param Zone $zone
     * @return self
     */
    public function addZone(Zone $zone): self
    {
        $this->zones[] = $zone;
        return $this;
    }

    /**
     *
     * @param array $zones
     * @return self
     */
    public function setZones(array $zones): self
    {
        $this->zones = $zones;
        return $this;
    }
}