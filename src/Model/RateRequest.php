<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use WorldOptions\Model\Shop\Address;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Localization\Currency;

final class RateRequest extends Model
{
    /**
     * @var Address $origin
     */
    protected ?Address $origin;

    /**
     * @var Address $destination
     */
    protected Address $destination;

    /**
     * @var array $items
     */
    protected array $items = [];

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     * @var array $shipments
     */
    protected bool $fontend = true;

    /**
     * @var bool $residental
     * 
     */
    private bool $residental = false;

    /**
     * @var ?bool $packing
     * 
     */
    private ?bool $packing = null;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @var int $order
     */
    protected int $order;

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Address|null
     */
    public function getOrigin(): ?Address
    {
        return isset($this->origin) ? $this->origin : null;
    }

    /**
     *
     * @param Address $origin
     * @return self
     */
    public function setOrigin(Address $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     *
     * @return Address
     */
    public function getDestination(): Address
    {
        return $this->destination;
    }

    /**
     *
     * @param Address $destination
     * @return self
     */
    public function setDestination(Address $destination): self
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @param Product $item
     * @return self
     */
    public function addItem(Product $item): self
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     *
     * @param array $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isFrontend(): bool
    {
        return $this->frontend;
    }

    /**
     *
     * @param bool $frontend
     * @return self
     */
    public function setFrontend(bool $frontend): self
    {
        $this->frontend = $frontend;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isResidental(): bool
    {
        return $this->residental;
    }

    /**
     *
     * @param bool $residental
     * @return self
     */
    public function setResidental(bool $residental): self
    {
        $this->residental = $residental;
        return $this;
    }

    /**
     *
     * @return ?bool
     */
    public function isPacking(): ?bool
    {
        return $this->packing;
    }

    /**
     *
     * @param ?bool $packing
     * @return self
     */
    public function setPacking(?bool $packing): self
    {
        $this->packing = $packing;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getOrder(): ?int
    {
        return isset($this->order) ? $this->order : null;
    }

    /**
     *
     * @param int $order
     * @return self
     */
    public function setOrder(?int $order): self
    {
        $this->order = $order;
        return $this;
    }
}