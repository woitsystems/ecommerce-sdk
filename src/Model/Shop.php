<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use WorldOptions\Model\Shop\Address;
use WorldOptions\Model\Shop\Box;
use WorldOptions\Model\Shop\BackupRate;
use WorldOptions\Model\Shop\CustomRate;
use WorldOptions\Model\Carrier\Service;
use WorldOptions\Model\Localization\Country;
use DateTime;

final class Shop extends Model
{
    public const PLATFORM_CUSTOM = 'custom';
    public const PLATFORM_SHOPIFY = 'shopify';
    public const PLATFORM_WOOCOMMERCE = 'woocommerce';
    public const PLATFORM_OPENCART = 'opencart';
    public const PLATFORM_MAGENTO = 'magento_1';
    public const PLATFORM_MAGENTO_2 = 'magento_2';
    public const PLATFORM_WIX = 'wix';
    public const PLATFORM_PRESTASHOP = 'prestashop';

    public const AVAILABLE_PLATFORMS = [
        self::PLATFORM_CUSTOM,
        self::PLATFORM_SHOPIFY,
        self::PLATFORM_WOOCOMMERCE,
        self::PLATFORM_OPENCART,
        self::PLATFORM_MAGENTO,
        self::PLATFORM_MAGENTO_2,
        self::PLATFORM_WIX,
        self::PLATFORM_PRESTASHOP,
    ];

    public const DATE_FORMAT_1 = 'D, d M Y';
    public const DATE_FORMAT_2 = 'd M Y';
    public const DATE_FORMAT_3 = 'D, M d Y';
    public const DATE_FORMAT_4 = 'M d Y';
    public const DATE_FORMAT_5 = 'm/d/Y';
    public const DATE_FORMAT_6 = 'd/m/Y';
    public const DATE_FORMAT_7 = 'm-d-Y';
    public const DATE_FORMAT_8 = 'd-m-Y';

    public const DATE_FORMATS = [
        self::DATE_FORMAT_1,
        self::DATE_FORMAT_2,
        self::DATE_FORMAT_3,
        self::DATE_FORMAT_4,
        self::DATE_FORMAT_5,
        self::DATE_FORMAT_6,
        self::DATE_FORMAT_7,
        self::DATE_FORMAT_8,
    ];

    public const TIME_FORMAT_1 = 'h:i A';
    public const TIME_FORMAT_2 = 'g:i A';
    public const TIME_FORMAT_3 = 'h:i a';
    public const TIME_FORMAT_4 = 'g:i a';
    public const TIME_FORMAT_5 = 'H:i';
    public const TIME_FORMAT_6 = 'G:i';

    public const TIME_FORMATS = [
        self::TIME_FORMAT_1,
        self::TIME_FORMAT_2,
        self::TIME_FORMAT_3,
        self::TIME_FORMAT_4,
        self::TIME_FORMAT_5,
        self::TIME_FORMAT_6,
    ];

    public const UNIT_WEIGHT_KG = 'kg';
    public const UNIT_WEIGHT_LB = 'lb';

    public const WEIGHT_UNITS = [
        self::UNIT_WEIGHT_KG,
        self::UNIT_WEIGHT_LB,
    ];

    public const UNIT_LENGTH_CM = 'cm';
    public const UNIT_LENGTH_INCH = 'inch';

    public const LENGTH_UNITS = [
        self::UNIT_LENGTH_CM,
        self::UNIT_LENGTH_INCH,
    ];

    public const PACKAGE_TYPE_NOT_DOCUMENT = 'Any_NonDocument';
    public const PACKAGE_TYPE_DOCUMENT = 'Any_Document';

    public const PACKAGE_TYPES = [
        self::PACKAGE_TYPE_NOT_DOCUMENT,
        self::PACKAGE_TYPE_DOCUMENT,
    ];

    public const INVOICE_TYPE_HELP_ME_GENERATE = 'Help_Me_Generate';
    public const INVOICE_TYPE_I_ALREADY_HAVE_ONE = 'I_Already_Have_One';

    public const INVOICE_TYPES = [
        self::INVOICE_TYPE_HELP_ME_GENERATE,
        self::INVOICE_TYPE_I_ALREADY_HAVE_ONE,
    ];

    public const DUTIES_PAYER_SENDER = 'Duties_To_Be_Paid_By_Sender';
    public const DUTIES_PAYER_RECEIVER = 'Duties_To_Be_Paid_By_Receiver';

    public const DUTIES_PAYER = [
        self::DUTIES_PAYER_SENDER,
        self::DUTIES_PAYER_RECEIVER,
    ];

    public const COLLECTION_TYPE_NEXT_DAY = 'next_day';
    public const COLLECTION_TYPE_SPECIFIC_DAY = 'specific_day';
    public const COLLECTION_TYPE_NO_COLLECTION = 'no_collection';

    public const COLLECTION_TYPES = [
        self::COLLECTION_TYPE_NEXT_DAY,
        self::COLLECTION_TYPE_SPECIFIC_DAY,
        self::COLLECTION_TYPE_NO_COLLECTION,
    ];
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $url
     */
    protected string $url;

    /**
     * @var Customer $customer
     */
    protected ?Customer $customer;

    /**
     * @var string $platform
     */
    protected string $platform;

    /**
     * @var string $dateFormat
     */
    protected string $dateFormat;

    /**
     * @var string $timeFormat
     */
    protected string $timeFormat;

    /**
     * @var string $weightUnit
     */
    protected string $weightUnit;

    /**
     * @var string $lengthUnit
     */
    protected string $lengthUnit;

    /**
     * @var bool $enableRates
     */
    protected bool $enableRates;

    /**
     * @var bool $enableAutobook
     */
    protected bool $enableAutobook;

    /**
     * @var boolean $enableDeliveryDropOff
     */
    protected $enableDeliveryDropOff;
    /**
     * @var boolean $enablePacking
     */
    protected $enablePacking;

    /**
     * @var bool $enableCustomBoxes
     */
    protected bool $enableCustomBoxes;

    /**
     * @var bool $displayTrackingNumber
     */
    protected bool $displayTrackingNumber;

    /**
     * @var bool $devMode
     */
    protected bool $devMode;

    /**
     * @var string $vatNumber
     */
    protected ?string $vatNumber;

    /**
     * @var string $eoriNumber
     */
    protected ?string $eoriNumber;

    /**
     * @var string $iossName
     */
    protected ?string $iossName;

    /**
     * @var string $iossNumber
     */
    protected ?string $iossNumber;

    /**
     * @var float $defaultWidth
     */
    protected float $defaultWidth;

    /**
     * @var float $defaultLength
     */
    protected float $defaultLength;

    /**
     * @var float $defaultDepth
     */
    protected float $defaultDepth;

    /**
     * @var float $defaultWeight
     */
    protected float $defaultWeight;

    /**
     * @var string $packageType
     */
    protected string $packageType;

    /**
     * @var string $dutiesPayer
     */
    protected string $dutiesPayer;

    /**
     * @var string $collectionType
     */
    protected string $collectionType;

    /**
     * @var string $invoiceType
     */
    protected string $invoiceType;

    /**
     * @var bool $paperlessInvoice
     */
    protected bool $paperlessInvoice;

    /**
     * @var bool $addTaxToRates
     */
    protected bool $addTaxToRates;

    /**
     * @var bool $showDeliveryDate
     */
    protected bool $showDeliveryDate;

    /**
     * @var int $collectionDay
     */
    protected ?int $collectionDay;

    /**
     * @var DateTime $collectionReadyTime
     */
    protected ?DateTime $collectionReadyTime;

    /**
     * @var DateTime $collectionCloseTime
     */
    protected ?DateTime $collectionCloseTime;

    /**
     * @var string $thermalPrinter
     */
    protected ?string $thermalPrinter;


    /**
     * @var array $thermalPrinterSettings
     */
    protected array $thermalPrinterSettings = [];

    /**
     * @var Address $address
     */
    protected ?Address $address;

    /**
     * @var Address $collectionAddress
     */
    protected ?Address $collectionAddress;

    /**
     * @var array $boxes
     */
    protected array $boxes;

    /**
     * @var array $countries
     */
    protected array $countries;

    /**
     * @var array $rates
     */
    protected array $rates;

    /**
     * @var array $services
     */
    protected array $services;

     /**
     * @var int $ratesTimeout
     */
    protected ?int $ratesTimeout;

    /**
     * @var bool $residentalAddress
     */
    protected bool $residentalAddress;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setUrl($data['url']);
            $this->setPlatform($data['platform']);
            $this->setDateFormat($data['dateFormat']);
            $this->setTimeFormat($data['timeFormat']);
            $this->setWeightUnit($data['weightUnit']);
            $this->setLengthUnit($data['lengthUnit']);
            $this->setEnableRates($data['enableRates']);
            $this->setEnableAutobook($data['enableAutobook']);
            $this->setEnableDeliveryDropOff($data['enableDeliveryDropOff']);
            $this->setEnablePacking($data['enablePacking']);
            $this->setEnableCustomBoxes($data['enableCustomBoxes']);
            $this->setDisplayTrackingNumber($data['displayTrackingNumber']);
            $this->setDevMode($data['devMode']);
            if (isset($data['vatNumber'])) {
                $this->setVatNumber($data['vatNumber']);
            }
            if (isset($data['eoriNumber'])) {
                $this->setEoriNumber($data['eoriNumber']);
            }
            if (isset($data['iossName'])) {
                $this->setIossName($data['iossName']);
            }
            if (isset($data['iossNumber'])) {
                $this->setIossNumber($data['iossNumber']);
            }

            if (isset($data['residentalAddress'])) {
                $this->setResidentalAddress($data['residentalAddress']);
            }

            $this->setDefaultWidth($data['defaultWidth']);
            $this->setDefaultLength($data['defaultLength']);
            $this->setDefaultDepth($data['defaultDepth']);
            $this->setDefaultWeight($data['defaultWeight']);

            $this->setDutiesPayer($data['dutiesPayer']);
            $this->setPackageType($data['packageType']);
            $this->setCollectionType($data['collectionType']);
            $this->setInvoiceType($data['invoiceType']);
            $this->setPaperlessInvoice($data['paperlessInvoice']);
            $this->setAddTaxToRates($data['addTaxToRates']);
            $this->setShowDeliveryDate($data['showDeliveryDate']);
            if (isset($data['collectionDay'])) {
                $this->setCollectionDay($data['collectionDay']);
            }
            if (isset($data['collectionReadyTime'])) {
                $this->setCollectionReadyTime(new DateTime($data['collectionReadyTime']));
            }
            if (isset($data['collectionCloseTime'])) {
                $this->setCollectionCloseTime(new DateTime($data['collectionCloseTime']));
            }
            if (isset($data['thermalPrinter'])) {
                $this->setThermalPrinter($data['thermalPrinter']);
            }
            if (isset($data['thermalPrinterSettings'])) {
                $this->setThermalPrinterSettings($data['thermalPrinterSettings']);
            }

            if (isset($data['customer']) && $data['customer']) {
                $customer = new Customer($data['customer']);
                $this->setCustomer($customer);
            }

            if (isset($data['address']) && $data['address']) {
                $address = new Address($data['address']);
                $this->setAddress($address);
            }

            if (isset($data['collectionAddress']) && $data['collectionAddress']) {
                $collectionAddress = new Address($data['collectionAddress']);
                $this->setCollectionAddress($collectionAddress);
            }

            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }

            if (isset($data['countries']) && $data['countries']) {
                foreach ($data['countries'] as $item) {
                    $country = new Country($item);
                    $this->addCountry($country);
                }
            }

            if (isset($data['customRates']) && $data['customRates']) {
                foreach ($data['customRates'] as $item) {
                    $rate = new CustomRate($item);
                    $this->addRate($rate);
                }
            }

            if (isset($data['backupRates']) && $data['backupRates']) {
                foreach ($data['backupRates'] as $item) {
                    $rate = new BackupRate($item);
                    $this->addBackupRate($rate);
                }
            }

            if (isset($data['carrierServices']) && $data['carrierServices']) {
                foreach ($data['carrierServices'] as $item) {
                    $service = new Service($item);
                    $this->addCarrierService($service);
                }
            }

            if (isset($data['ratesTimeout'])) {
                $this->setRatesTimeout($data['ratesTimeout']);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     *
     * @param string $url
     * @return self
     */
    public function setUrl(string $url): self
    {
        $this->url = str_replace(['www.', 'https://', 'http://'], '', $url);
        return $this;
    }

    /**
     *
     * @return Customer|null
     */
    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    /**
     *
     * @param Customer $customer
     * @return self
     */
    public function setCustomer(Customer $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPlatform(): string
    {
        return in_array($this->platform, self::AVAILABLE_PLATFORMS) ? $this->platform : self::PLATFORM_CUSTOM;
    }

    /**
     *
     * @param string $platform
     * @return self
     */
    public function setPlatform(string $platform): self
    {
        $this->platform = in_array($platform, self::AVAILABLE_PLATFORMS) ? $platform : self::PLATFORM_CUSTOM;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDateFormat(): string
    {
        return in_array($this->dateFormat, self::DATE_FORMATS) ? $this->dateFormat : self::DATE_FORMAT_1;
    }

    /**
     *
     * @param string $dateFormat
     * @return self
     */
    public function setDateFormat(string $dateFormat): self
    {
        $this->dateFormat = in_array($dateFormat, self::DATE_FORMATS) ? $dateFormat : self::DATE_FORMAT_1;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTimeFormat(): string
    {
        return in_array($this->timeFormat, self::TIME_FORMATS) ? $this->timeFormat : self::TIME_FORMAT_1;
    }

    /**
     *
     * @param string $timeFormat
     * @return self
     */
    public function setTimeFormat(string $timeFormat): self
    {
        $this->timeFormat = in_array($timeFormat, self::TIME_FORMATS) ? $timeFormat : self::TIME_FORMAT_1;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getWeightUnit(): string
    {
        return in_array($this->weightUnit, self::WEIGHT_UNITS) ? $this->weightUnit : self::UNIT_WEIGHT_KG;
    }

    /**
     *
     * @param string $weightUnit
     * @return self
     */
    public function setWeightUnit(string $weightUnit): self
    {
        $this->weightUnit = in_array($weightUnit, self::WEIGHT_UNITS) ? $weightUnit : self::UNIT_WEIGHT_KG;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLengthUnit(): string
    {
        return in_array($this->lengthUnit, self::LENGTH_UNITS) ? $this->lengthUnit : self::UNIT_LENGTH_CM;
    }

    /**
     *
     * @param string $lengthUnit
     * @return self
     */
    public function setLengthUnit(string $lengthUnit): self
    {
        $this->lengthUnit = in_array($lengthUnit, self::LENGTH_UNITS) ? $lengthUnit : self::UNIT_LENGTH_CM;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnableRates(): bool
    {
        return isset($this->enableRates) ? $this->enableRates : false;
    }

    /**
     *
     * @param bool $enableRates
     * @return self
     */
    public function setEnableRates(bool $enableRates): self
    {
        $this->enableRates = $enableRates;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnableCustomBoxes(): bool
    {
        return isset($this->enableCustomBoxes) ? $this->enableCustomBoxes : false;
    }

    /**
     *
     * @param bool $enableCustomBoxes
     * @return self
     */
    public function setEnableCustomBoxes(bool $enableCustomBoxes): self
    {
        $this->enableCustomBoxes = $enableCustomBoxes;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnableAutobook(): bool
    {
        return isset($this->enableAutobook) ? $this->enableAutobook : false;
    }

    /**
     *
     * @param bool $enableAutobook
     * @return self
     */
    public function setEnableAutobook(bool $enableAutobook): self
    {
        $this->enableAutobook = $enableAutobook;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnableDeliveryDropOff(): bool
    {
        return $this->enableDeliveryDropOff;
    }

    /**
     *
     * @param bool $enableDeliveryDropOff
     * @return self
     */
    public function setEnableDeliveryDropOff(bool $enableDeliveryDropOff): self
    {
        $this->enableDeliveryDropOff = $enableDeliveryDropOff;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnablePacking(): bool
    {
        return $this->enablePacking;
    }

    /**
     *
     * @param bool $enablePacking
     * @return self
     */
    public function setEnablePacking(bool $enablePacking): self
    {
        $this->enablePacking = $enablePacking;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isDisplayTrackingNumber(): bool
    {
        return isset($this->displayTrackingNumber) ? $this->displayTrackingNumber : false;
    }

    /**
     *
     * @param bool $displayTrackingNumber
     * @return self
     */
    public function setDisplayTrackingNumber(bool $displayTrackingNumber): self
    {
        $this->displayTrackingNumber = $displayTrackingNumber;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isDevMode(): bool
    {
        return isset($this->devMode) ? $this->devMode : false;
    }

    /**
     *
     * @param bool $devMode
     * @return self
     */
    public function setDevMode(bool $devMode): self
    {
        $this->devMode = $devMode;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return isset($this->vatNumber) ? $this->vatNumber : null;
    }

    /**
     *
     * @param string $vatNumber
     * @return self
     */
    public function setVatNumber(?string $vatNumber): self
    {
        $this->vatNumber = $vatNumber;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getEoriNumber(): ?string
    {
        return isset($this->eoriNumber) ? $this->eoriNumber : null;
    }

    /**
     *
     * @param string $eoriNumber
     * @return self
     */
    public function setEoriNumber(?string $eoriNumber): self
    {
        $this->eoriNumber = $eoriNumber;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getIossName(): ?string
    {
        return isset($this->iossName) ? $this->iossName : null;
    }

    /**
     *
     * @param string $iossName
     * @return self
     */
    public function setIossName(?string $iossName): self
    {
        $this->iossName = $iossName;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getIossNumber(): ?string
    {
        return isset($this->iossNumber) ? $this->iossNumber : null;
    }

    /**
     *
     * @param string $iossNumber
     * @return self
     */
    public function setIossNumber(?string $iossNumber): self
    {
        $this->iossNumber = $iossNumber;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDefaultWidth(): float
    {
        return $this->defaultWidth;
    }

    /**
     *
     * @param float $defaultWidth
     * @return self
     */
    public function setDefaultWidth(float $defaultWidth): self
    {
        $this->defaultWidth = $defaultWidth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDefaultLength(): float
    {
        return $this->defaultLength;
    }

    /**
     *
     * @param float $defaultLength
     * @return self
     */
    public function setDefaultLength(float $defaultLength): self
    {
        $this->defaultLength = $defaultLength;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDefaultDepth(): float
    {
        return $this->defaultDepth;
    }

    /**
     *
     * @param float $defaultDepth
     * @return self
     */
    public function setDefaultDepth(float $defaultDepth): self
    {
        $this->defaultDepth = $defaultDepth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDefaultWeight(): float
    {
        return $this->defaultWeight;
    }

    /**
     *
     * @param float $defaultWeight
     * @return self
     */
    public function setDefaultWeight(float $defaultWeight): self
    {
        $this->defaultWeight = $defaultWeight;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPackageType(): string
    {
        return in_array($this->packageType, self::PACKAGE_TYPES) ? $this->packageType : self::PACKAGE_TYPE_NOT_DOCUMENT;
    }

    /**
     *
     * @param string $packageType
     * @return self
     */
    public function setPackageType(string $packageType): self
    {
        $this->packageType = in_array($packageType, self::PACKAGE_TYPES) ? $packageType : self::PACKAGE_TYPE_NOT_DOCUMENT;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDutiesPayer(): string
    {
        return in_array($this->dutiesPayer, self::DUTIES_PAYER) ? $this->dutiesPayer : self::DUTIES_PAYER_SENDER;
    }

    /**
     *
     * @param string $dutiesPayer
     * @return self
     */
    public function setDutiesPayer(string $dutiesPayer): self
    {
        $this->dutiesPayer = in_array($dutiesPayer, self::DUTIES_PAYER) ? $dutiesPayer : self::DUTIES_PAYER_SENDER;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCollectionType(): string
    {
        return in_array($this->collectionType, self::COLLECTION_TYPES) ? $this->collectionType : self::COLLECTION_TYPE_NEXT_DAY;
    }

    /**
     *
     * @param string $collectionType
     * @return self
     */
    public function setCollectionType(string $collectionType): self
    {
        $this->collectionType = in_array($collectionType, self::COLLECTION_TYPES) ? $collectionType : self::COLLECTION_TYPE_NEXT_DAY;
        return $this;
    }

    /**
     *
     * @param string $invoiceType
     * @return self
     */
    public function setInvoiceType(string $invoiceType): self
    {
        $this->invoiceType = in_array($invoiceType, self::INVOICE_TYPES) ? $invoiceType : self::INVOICE_TYPE_HELP_ME_GENERATE;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getInvoiceType(): string
    {
        return in_array($this->invoiceType, self::INVOICE_TYPES) ? $this->invoiceType : self::INVOICE_TYPE_HELP_ME_GENERATE;
    }

    /**
     *
     * @return bool
     */
    public function isPaperlessInvoice(): bool
    {
        return isset($this->paperlessInvoice) ? $this->paperlessInvoice : false;
    }

    /**
     *
     * @param bool $paperlessInvoice
     * @return self
     */
    public function setPaperlessInvoice(bool $paperlessInvoice): self
    {
        $this->paperlessInvoice = $paperlessInvoice;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isAddTaxToRates(): bool
    {
        return isset($this->addTaxToRates) ? $this->addTaxToRates : false;
    }

    /**
     *
     * @param bool $addTaxToRates
     * @return self
     */
    public function setAddTaxToRates(bool $addTaxToRates): self
    {
        $this->addTaxToRates = $addTaxToRates;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isShowDeliveryDate(): bool
    {
        return isset($this->showDeliveryDate) ? $this->showDeliveryDate : false;
    }

    /**
     *
     * @param bool $showDeliveryDate
     * @return self
     */
    public function setShowDeliveryDate(bool $showDeliveryDate): self
    {
        $this->showDeliveryDate = $showDeliveryDate;
        return $this;
    }

    /**
     *
     * @param int $collectionDay
     * @return self
     */
    public function setCollectionDay(?int $collectionDay): self
    {
        $this->collectionDay = $collectionDay;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getCollectionDay(): ?int
    {
        return isset($this->collectionDay) ? $this->collectionDay : null;
    }

    /**
     *
     * @return DateTime|null
     */
    public function getCollectionReadyTime(): ?DateTime
    {
        return isset($this->collectionReadyTime) ? $this->collectionReadyTime : null;
    }

    /**
     *
     * @param ?DateTime $collectionReadyTime
     * @return self
     */
    public function setCollectionReadyTime(?DateTime $collectionReadyTime): self
    {
        $this->collectionReadyTime = $collectionReadyTime;
        return $this;
    }

    /**
     *
     * @return DateTime|null
     */
    public function getCollectionCloseTime(): ?DateTime
    {
        return isset($this->collectionCloseTime) ? $this->collectionCloseTime : null;
    }

    /**
     *
     * @param ?DateTime $collectionCloseTime
     * @return self
     */
    public function setCollectionCloseTime(?DateTime $collectionCloseTime): self
    {
        $this->collectionCloseTime = $collectionCloseTime;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getThermalPrinter(): ?string
    {
        return isset($this->thermalPrinter) ? $this->thermalPrinter : '';
    }

    /**
     *
     * @param string $thermalPrinter
     * @return self
     */
    public function setThermalPrinter(?string $thermalPrinter): self
    {
        $this->thermalPrinter = $thermalPrinter;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getThermalPrinterSettings(): array
    {
        return isset($this->thermalPrinterSettings) ? $this->thermalPrinterSettings : [];
    }

    /**
     *
     * @param array $thermalPrinterSettings
     * @return self
     */
    public function setThermalPrinterSettings(array $thermalPrinterSettings): self
    {
        $this->thermalPrinterSettings = $thermalPrinterSettings;
        return $this;
    }

    /**
     *
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return isset($this->address) ? $this->address : null;
    }

    /**
     *
     * @param Address $address
     * @return self
     */
    public function setAddress(Address $address): self
    {
        $this->address = $address;
        return $this;
    }

    /**
     *
     * @return Address|null
     */
    public function getCollectionAddress(): ?Address
    {
        return isset($this->collectionAddress) ? $this->collectionAddress : null;
    }

    /**
     *
     * @param Address $collectionAddress
     * @return self
     */
    public function setCollectionAddress(Address $collectionAddress): self
    {
        $this->collectionAddress = $collectionAddress;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getBoxes(): ?array
    {
        return isset($this->boxes) ? $this->boxes : null;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getRates(): ?array
    {
        return isset($this->rates) ? $this->rates : null;
    }

    /**
     *
     * @param CustomRate $rate
     * @return self
     */
    public function addRate(CustomRate $rate): self
    {
        $this->rates[] = $rate;
        return $this;
    }

    /**
     *
     * @param array $rates
     * @return self
     */
    public function setRates(array $rates): self
    {
        $this->rates = $rates;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getBackupRates(): ?array
    {
        return isset($this->backupRates) ? $this->backupRates : null;
    }

    /**
     *
     * @param BackupRate $rate
     * @return self
     */
    public function addBackupRate(BackupRate $rate): self
    {
        $this->backupRates[] = $rate;
        return $this;
    }

    /**
     *
     * @param array $rates
     * @return self
     */
    public function setBackupRates(array $rates): self
    {
        $this->backupRates = $rates;
        return $this;
    }

    /**
     *
     * @return array|null
     */
    public function getCarrierServices(): ?array
    {
        return isset($this->services) ? $this->services : null;
    }

    /**
     *
     * @param Service $service
     * @return self
     */
    public function addCarrierService(Service $service): self
    {
        $this->services[] = $service;
        return $this;
    }

    /**
     *
     * @param array $services
     * @return self
     */
    public function setCarrierServices(array $services): self
    {
        $this->services = $services;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getCountries(): array
    {
        return isset($this->countries) ? $this->countries : [];
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function addCountry(Country $country): self
    {
        $this->countries[] = $country;
        return $this;
    }

    /**
     *
     * @param array $countries
     * @return self
     */
    public function setCountries(array $countries): self
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getRatesTimeout(): int
    {
        return $this->ratesTimeout;
    }

    /**
     *
     * @param int $ratesTimeout
     * @return self
     */
    public function setRatesTimeout(int $ratesTimeout): self
    {
        $this->ratesTimeout = $ratesTimeout;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isResidentalAddress(): bool
    {
        return isset($this->residentalAddress) ? $this->residentalAddress : false;
    }

    /**
     *
     * @param bool $residentalAddress
     * @return self
     */
    public function setResidentalAddress(bool $residentalAddress): self
    {
        $this->residentalAddress = $residentalAddress;
        return $this;
    }
}