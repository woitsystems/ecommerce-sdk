<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use WorldOptions\Model\Localization\Country;
use WorldOptions\Model\Shop;
use WorldOptions\Model\Shop\Box;

final class Customer extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $username
     */
    protected string $username;

    /**
     * @var string $password
     */
    protected string $password;

    /**
     * @var string $meternumber
     */
    protected string $meternumber;

    /**
     * @var Country $country
     */
    protected Country $country;

    /**
     * @var array $shops
     */
    protected array $shops = [];

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setUsername($data['username']);
            
            if (isset($data['country']) && $data['country']) {
                $country = new Country($data['country']);
                $this->setCountry($country);
            }

            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }

            if (isset($data['shops']) && $data['shops']) {
                foreach ($data['shops'] as $item) {
                    $shop = new Shop($item);
                    $this->addShop($shop);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     *
     * @param string $username
     * @return self
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     *
     * @param string $password
     * @return self
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getMeternumber(): string
    {
        return $this->meternumber;
    }

    /**
     *
     * @param string $meternumber
     * @return self
     */
    public function setMeternumber(string $meternumber): self
    {
        $this->meternumber = $meternumber;
        return $this;
    }

    /**
     *
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getShops(): array
    {
        return $this->shops;
    }

    /**
     *
     * @param Shop $shop
     * @return self
     */
    public function addShop(Shop $shop): self
    {
        $this->shops[] = $shop;
        return $this;
    }

    /**
     *
     * @param array $shops
     * @return self
     */
    public function setShops(array $shops): self
    {
        $this->shops = $shops;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }
}