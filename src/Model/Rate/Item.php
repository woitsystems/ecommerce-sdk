<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Rate\Box;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Model;

final class Item extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var Product $product
     */
    protected Product $product;

    /**
     * @var float $width
     */
    protected float $width;

    /**
     * @var float $length
     */
    protected float $length;

    /**
     * @var float $depth
     */
    protected float $depth;

    /**
     * @var float $weight
     */
    protected float $weight;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var string $rate
     */
    protected string $rate;

    /**
     * @var Currency $currency
     */
    protected ?Currency $currency;

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setWidth($data['width']);
            $this->setLength($data['length']);
            $this->setDepth($data['depth']);
            $this->setWeight($data['weight']);
            $this->setAmount($data['amount']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['product']) && $data['product']) {
                $product = new Product($data['product']);
                $this->setProduct($product);
            }

            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     *
     * @param Product $product
     * @return self
     */
    public function setProduct(Product $product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     *
     * @param float $width
     * @return self
     */
    public function setWidth(float $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     *
     * @param float $length
     * @return self
     */
    public function setLength(float $length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDepth(): float
    {
        return $this->depth;
    }

    /**
     *
     * @param float $depth
     * @return self
     */
    public function setDepth(float $depth): self
    {
        $this->depth = $depth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     *
     * @param float $weight
     * @return self
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getRate(): ?string
    {
        return $this->rate;
    }

    /**
     *
     * @param string $rate
     * @return self
     */
    public function setRate(?string $rate): self
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }
}