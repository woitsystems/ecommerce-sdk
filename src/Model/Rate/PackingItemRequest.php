<?php
/*
 * @since 1.4.0
 * @copyright Copyright (C) 2024 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Model;

final class PackingItemRequest extends Model
{
    /**
     * @var array $items
     */
    protected array $items = [];

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @param Product $item
     * @return self
     */
    public function addItem(Product $item): self
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     *
     * @param array $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }
}