<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Shop\Address;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Model;

final class RatesRequest extends Model
{
    /**
     * @var array $data
     */
    protected array $data = [];

    /**
     * @var array $shipments
     */
    protected bool $fontend = true;

    /**
     * @var bool $residental
     * 
     */
    private bool $residental = false;

    /**
     * @var ?bool $packing
     * 
     */
    private ?bool $packing = null;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @var int $order
     */
    protected int $order;

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getData(): ?array
    {
        return isset($this->data) ? $this->data : [];
    }

    /**
     *
     * @param array $data
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     *
     * @param RatesItemRequest $item
     * @return self
     */
    public function addData(RatesItemRequest $item): self
    {
        $this->data[] = $item;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isFrontend(): bool
    {
        return $this->frontend;
    }

    /**
     *
     * @param bool $frontend
     * @return self
     */
    public function setFrontend(bool $frontend): self
    {
        $this->frontend = $frontend;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isResidental(): bool
    {
        return $this->residental;
    }

    /**
     *
     * @param bool $residental
     * @return self
     */
    public function setResidental(bool $residental): self
    {
        $this->residental = $residental;
        return $this;
    }

    /**
     *
     * @return ?bool
     */
    public function isPacking(): ?bool
    {
        return $this->packing;
    }

    /**
     *
     * @param ?bool $packing
     * @return self
     */
    public function setPacking(?bool $packing): self
    {
        $this->packing = $packing;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getOrder(): ?int
    {
        return isset($this->order) ? $this->order : null;
    }

    /**
     *
     * @param int $order
     * @return self
     */
    public function setOrder(?int $order): self
    {
        $this->order = $order;
        return $this;
    }
}