<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Model;
use WorldOptions\Model\Shop\CustomRate;
use WorldOptions\Model\Localization\Currency;

final class RateCustom extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var CustomRate $customRate
     */
    protected CustomRate $customRate;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setAmount($data['amount']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['customRate']) && $data['customRate']) {
                $customRate = new CustomRate($data['customRate']);
                $this->setCustomRate($customRate);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return CustomRate
     */
    public function getCustomRate(): CustomRate
    {
        return $this->customRate;
    }

    /**
     *
     * @param CustomRate $customRate
     * @return self
     */
    public function setCustomRate(CustomRate $customRate): self
    {
        $this->customRate = $customRate;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }
}