<?php
/*
 * @since 1.2.8
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Model;
use WorldOptions\Model\Shop\BackupRate;
use WorldOptions\Model\Localization\Currency;

final class RateBackup extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var BackupRate $backupRate
     */
    protected BackupRate $backupRate;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setAmount($data['amount']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['backupRate']) && $data['backupRate']) {
                $backupRate = new BackupRate($data['backupRate']);
                $this->setBackupRate($backupRate);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return BackupRate
     */
    public function getBackupRate(): BackupRate
    {
        return $this->backupRate;
    }

    /**
     *
     * @param BackupRate $backupRate
     * @return self
     */
    public function setBackupRate(BackupRate $backupRate): self
    {
        $this->backupRate = $backupRate;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }
}