<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Model;
use WorldOptions\Model\Rate\Box;
use WorldOptions\Model\Rate\Carrier as RateCarrier;

final class RateExternal extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $rate
     */
    protected string $rate;

    /**
     * @var bool $success
     */
    protected bool $success;

    /**
     * @var float $responseTime
     */
    protected float $responseTime;

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     * @var array $carriersServices
     */
    protected array $carriersServices = [];

    /**
     * @var bool $residentalAddress
     */
    protected bool $residentalAddress;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            if (isset($data['rate']) && $data['rate']) {
                $this->setRate($data['rate']);
            }
            $this->setSuccess($data['success']);
            $this->setResponseTime($data['responseTime']);
    
            if (isset($data['boxes']) && $data['boxes']) {
                foreach ($data['boxes'] as $item) {
                    $box = new Box($item);
                    $this->addBox($box);
                }
            }

            if (isset($data['carriersServices']) && $data['carriersServices']) {
                foreach ($data['carriersServices'] as $item) {
                    $rateCarrier = new RateCarrier($item);
                    $this->addCarrierService($rateCarrier);
                }
            }

            if (isset($data['residentalAddress'])) {
                $this->setResidentalAddress($data['residentalAddress']);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getRate(): string
    {
        return $this->rate;
    }

    /**
     *
     * @param string $rate
     * @return self
     */
    public function setRate(string $rate): self
    {
        $this->rate = $rate;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     *
     * @param bool $success
     * @return self
     */
    public function setSuccess(bool $success): self
    {
        $this->success = $success;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getResponseTime(): float
    {
        return $this->responseTime;
    }

    /**
     *
     * @param float $responseTime
     * @return self
     */
    public function setResponseTime(float $responseTime): self
    {
        $this->responseTime = $responseTime;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getCarriersServices(): array
    {
        return $this->carriersServices;
    }

    /**
     *
     * @param RateCarrier $carrierService
     * @return self
     */
    public function addCarrierService(RateCarrier $carrierService): self
    {
        $this->carriersServices[] = $carrierService;
        return $this;
    }

    /**
     *
     * @param array $carriersServices
     * @return self
     */
    public function setCarriersServices(array $carriersServices): self
    {
        $this->carriersServices = $carriersServices;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isResidentalAddress(): bool
    {
        return isset($this->residentalAddress) ? $this->residentalAddress : false;
    }

    /**
     *
     * @param bool $residentalAddress
     * @return self
     */
    public function setResidentalAddress(bool $residentalAddress): self
    {
        $this->residentalAddress = $residentalAddress;
        return $this;
    }
}