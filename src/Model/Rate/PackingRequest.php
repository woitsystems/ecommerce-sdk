<?php
/*
 * @since 1.4.0
 * @copyright Copyright (C) 2024 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Localization\Currency;
use WorldOptions\Model\Model;

final class PackingRequest extends Model
{
    /**
     * @var array $data
     */
    protected array $data = [];

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     *
     * @return array
     */
    public function getData(): ?array
    {
        return isset($this->data) ? $this->data : [];
    }

    /**
     *
     * @param array $data
     * @return self
     */
    public function setData(array $data): self
    {
        $this->data = $data;
        return $this;
    }

    /**
     *
     * @param PackingItemRequest $item
     * @return self
     */
    public function addData(PackingItemRequest $item): self
    {
        $this->data[] = $item;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }
}