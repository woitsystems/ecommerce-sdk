<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Model;

final class Box extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $box
     */
    protected string $box;

    /**
     * @var float $width
     */
    protected float $width;

    /**
     * @var float $length
     */
    protected float $length;

    /**
     * @var float $depth
     */
    protected float $depth;

    /**
     * @var float $weight
     */
    protected float $weight;

    /**
     * @var array $items
     */
    protected array $items;

    /**
     * @var array $itemsPositions
     */
    protected array $itemsPositions;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            if (isset($data['id'])) {
                $this->setId($data['id']);
            }
            if (isset($data['box'])) {
                $this->setBox($data['box']);
            }
            $this->setWidth($data['width']);
            $this->setLength($data['length']);
            $this->setDepth($data['depth']);
            $this->setWeight($data['weight']);
            $this->setItemsPositions($data['itemsPositions']);

            if (isset($data['items']) && $data['items']) {
                foreach ($data['items'] as $item) {
                    $rateItem = new Item($item);
                    $this->addItem($rateItem);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getBox(): string
    {
        return $this->box;
    }

    /**
     *
     * @param string $box
     * @return self
     */
    public function setBox(string $box): self
    {
        $this->box = $box;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     *
     * @param float $width
     * @return self
     */
    public function setWidth(float $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getLength(): float
    {
        return $this->length;
    }

    /**
     *
     * @param float $length
     * @return self
     */
    public function setLength(float $length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getDepth(): float
    {
        return $this->depth;
    }

    /**
     *
     * @param float $depth
     * @return self
     */
    public function setDepth(float $depth): self
    {
        $this->depth = $depth;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     *
     * @param float $weight
     * @return self
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @param Item $item
     * @return self
     */
    public function addItem(Item $item): self
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     *
     * @param array $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getItemsPositions(): array
    {
        return $this->itemsPositions;
    }

    /**
     *
     * @param array $itemsPositions
     * @return self
     */
    public function setItemsPositions(array $itemsPositions): self
    {
        $this->itemsPositions = $itemsPositions;
        return $this;
    }
}