<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Model;
use WorldOptions\Model\Carrier\Service;
use WorldOptions\Model\Localization\Currency;

final class Carrier extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var Service $service
     */
    protected Service $service;

    /**
     * @var float $amount
     */
    protected float $amount;

    /**
     * @var ?string $package
     */
    protected ?string $package;

    /**
     * @var ?string $delivery
     */
    protected ?string $delivery;

    /**
     * @var Currency $currency
     */
    protected Currency $currency;

    /**
     * @var array $dropOffPoint
     */
    protected array $dropOffPoint;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setAmount($data['amount']);
            $this->setPackage($data['package']);
            $this->setDelivery($data['delivery']);
            $this->setDropOffPoint($data['dropOffPoint']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }
            
            if (isset($data['carrierService']) && $data['carrierService']) {
                $carrierService = new Service($data['carrierService']);
                $this->setService($carrierService);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Service
     */
    public function getService(): Service
    {
        return $this->service;
    }

    /**
     *
     * @param Service $service
     * @return self
     */
    public function setService(Service $service): self
    {
        $this->service = $service;
        return $this;
    }

    /**
     *
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     *
     * @param float $amount
     * @return self
     */
    public function setAmount(float $amount): self
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param Currency $currency
     * @return self
     */
    public function setCurrency(Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getPackage(): ?string
    {
        return $this->package;
    }

    /**
     *
     * @param ?string $package
     * @return self
     */
    public function setPackage(?string $package): self
    {
        $this->package = $package;
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getDelivery(): ?string
    {
        return $this->delivery;
    }

    /**
     *
     * @param ?string $delivery
     * @return self
     */
    public function setDelivery(?string $delivery): self
    {
        $this->delivery = $delivery;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getDropOffPoint(): array
    {
        return $this->dropOffPoint;
    }

    /**
     *
     * @param array $dropOffPoint
     * @return self
     */
    public function setDropOffPoint(array $dropOffPoint): self
    {
        $this->dropOffPoint = $dropOffPoint;
        return $this;
    }
}