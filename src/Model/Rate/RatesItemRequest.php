<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Rate;

use WorldOptions\Model\Shop\Address;
use WorldOptions\Model\Shop\Product;
use WorldOptions\Model\Model;

final class RatesItemRequest extends Model
{
    /**
     * @var Address $origin
     */
    protected ?Address $origin;

    /**
     * @var Address $destination
     */
    protected Address $destination;

    /**
     * @var array $items
     */
    protected array $items = [];

    /**
     * @var array $boxes
     */
    protected array $boxes = [];

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Address|null
     */
    public function getOrigin(): ?Address
    {
        return isset($this->origin) ? $this->origin : null;
    }

    /**
     *
     * @param Address $origin
     * @return self
     */
    public function setOrigin(Address $origin): self
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     *
     * @return Address
     */
    public function getDestination(): Address
    {
        return $this->destination;
    }

    /**
     *
     * @param Address $destination
     * @return self
     */
    public function setDestination(Address $destination): self
    {
        $this->destination = $destination;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @param Product $item
     * @return self
     */
    public function addItem(Product $item): self
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     *
     * @param array $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBoxes(): array
    {
        return $this->boxes;
    }

    /**
     *
     * @param Box $box
     * @return self
     */
    public function addBox(Box $box): self
    {
        $this->boxes[] = $box;
        return $this;
    }

    /**
     *
     * @param array $boxes
     * @return self
     */
    public function setBoxes(array $boxes): self
    {
        $this->boxes = $boxes;
        return $this;
    }
}