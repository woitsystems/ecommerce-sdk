<?php
/*
 * @since 1.1.7
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Carrier;

use WorldOptions\Model\Carrier;
use WorldOptions\Model\Model;
use WorldOptions\Model\Shop;
use DateTime;

final class Config extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var bool $displayName
     */
    protected bool $displayName;

    /**
     * @var string $alias
     */
    protected string $alias;

    /**
     * @var Carrier $carrier
     */
    protected Carrier $carrier;

    /**
     * @var string $collectionType
     */
    protected $collectionType;

    /**
     * @var int $collectionDay
     */
    protected $collectionDay;

    /**
     * @var DateTime $collectionReadyTime
     */
    protected $collectionReadyTime;

    /**
     * @var DateTime $collectionCloseTime
     */
    protected $collectionCloseTime;

    /**
     * @var bool $enableFrontend
     */
    protected bool $enableFrontend;

    /**
     * @var bool $enableBackend
     */
    protected bool $enableBackend;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);

            $this->setDisplayName($data['displayName']);

            if (isset($data['enableFrontend'])) {
                $this->setEnableFrontend($data['enableFrontend']);
            }

            if (isset($data['enableBackend'])) {
                $this->setEnableBackend($data['enableBackend']);
            }

            if (isset($data['alias']) && $data['alias']) {
                $this->setAlias($data['alias']);
            }

            if (isset($data['collectionType']) && $data['collectionType']) {
                $this->setCollectionType($data['collectionType']);
            }

            if (isset($data['collectionDay']) && $data['collectionDay']) {
                $this->setCollectionDay($data['collectionDay']);
            }

            if (isset($data['collectionReadyTime']) && $data['collectionReadyTime']) {
                $this->setCollectionReadyTime(new DateTime($data['collectionReadyTime']));
            }

            if (isset($data['collectionCloseTime']) && $data['collectionCloseTime']) {
                $this->setCollectionCloseTime(new DateTime($data['collectionCloseTime']));
            }

            if (isset($data['carrier']) && $data['carrier']) {
                $carrier = new Carrier($data['carrier']);
                $this->setCarrier($carrier);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isDisplayName(): bool
    {
        return isset($this->displayName) ? $this->displayName : true;
    }

    /**
     *
     * @param string $displayName
     * @return self
     */
    public function setDisplayName(?bool $displayName): self
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getAlias(): ?string
    {
        return isset($this->alias) ? $this->alias : null;
    }

    /**
     *
     * @param string $alias
     * @return self
     */
    public function setAlias(string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     *
     * @return Carrier
     */
    public function getCarrier(): Carrier
    {
        return $this->carrier;
    }

    /**
     *
     * @param Carrier $carrier
     * @return self
     */
    public function setCarrier(Carrier $carrier): self
    {
        $this->carrier = $carrier;
        return $this;
    }

    /**
     *
     * @return int|null
     */
    public function getCollectionDay(): ?int
    {
        return $this->collectionDay;
    }

    /**
     *
     * @param int $collectionDay
     * @return self
     */
    public function setCollectionDay(?int $collectionDay): self
    {
        $this->collectionDay = $collectionDay;
        return $this;
    }

    /**
     *
     * @return DateTime|null
     */
    public function getCollectionReadyTime(): ?DateTime
    {
        return $this->collectionReadyTime;
    }

    /**
     *
     * @param ?DateTime $collectionReadyTime
     * @return self
     */
    public function setCollectionReadyTime(?DateTime $collectionReadyTime): self
    {
        $this->collectionReadyTime = $collectionReadyTime;
        return $this;
    }

    /**
     *
     * @return DateTime|null
     */
    public function getCollectionCloseTime(): ?DateTime
    {
        return $this->collectionCloseTime;
    }

    /**
     *
     * @param ?DateTime $collectionCloseTime
     * @return self
     */
    public function setCollectionCloseTime(?DateTime $collectionCloseTime): self
    {
        $this->collectionCloseTime = $collectionCloseTime;
        return $this;
    }

    /**
     *
     * @param string $collectionType
     * @return self
     */
    public function setCollectionType(?string $collectionType): self
    {
        if ($collectionType == null || in_array($collectionType, Shop::COLLECTION_TYPES)) {
            $this->collectionType = $collectionType;
        } else {
            $this->collectionType = Shop::COLLECTION_TYPE_NEXT_DAY;
        }
        return $this;
    }

    /**
     *
     * @return string|null
     */
    public function getCollectionType(): ?string
    {
        return $this->collectionType;
    }

    /**
     *
     * @return bool
     */
    public function isEnableFrontend(): bool
    {
        return isset($this->enableFrontend) ? $this->enableFrontend : true;
    }

    /**
     *
     * @param string $enableFrontend
     * @return self
     */
    public function setEnableFrontend(?bool $enableFrontend): self
    {
        $this->enableFrontend = $enableFrontend;
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function isEnableBackend(): bool
    {
        return isset($this->enableBackend) ? $this->enableBackend : true;
    }

    /**
     *
     * @param string $enableBackend
     * @return self
     */
    public function setEnableBackend(?bool $enableBackend): self
    {
        $this->enableBackend = $enableBackend;
        return $this;
    }
}