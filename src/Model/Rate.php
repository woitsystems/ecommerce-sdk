<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use WorldOptions\Model\Shop\Address;
use WorldOptions\Model\Shop\Shipment;
use WorldOptions\Model\Shop\Order;
use WorldOptions\Model\Rate\Item;
use WorldOptions\Model\Rate\RateCustom;
use WorldOptions\Model\Rate\RateBackup;
use WorldOptions\Model\Rate\RateExternal;
use WorldOptions\Model\Rate\RateInternal;

final class Rate extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var Address $addressFrom
     */
    protected Address $addressFrom;

    /**
     * @var Address $addressTo
     */
    protected Address $addressTo;

    /**
     * @var array $items
     */
    protected array $items = [];

    /**
     * @var array $internalRates
     */
    protected array $internalRates = [];

    /**
     * @var array $customRates
     */
    protected array $customRates = [];

    /**
     * @var array $backupRates
     */
    protected array $backupRates = [];

    /**
     * @var array $externalRates
     */
    protected array $externalRates = [];

    /**
     * @var array $shipments
     */
    protected array $shipments = [];

    /**
     * @var Order $order
     */
    protected Order $order;

    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);

            if (isset($data['addressFrom']) && $data['addressFrom']) {
                $address = new Address($data['addressFrom']);
                $this->setAddressFrom($address);
            }

            if (isset($data['addressTo']) && $data['addressTo']) {
                $address = new Address($data['addressTo']);
                $this->setAddressTo($address);
            }

            if (isset($data['items']) && $data['items']) {
                foreach ($data['items'] as $item) {
                    $rateItem = new Item($item);
                    $this->addItem($rateItem);
                }
            }

            if (isset($data['webservicesRates']) && $data['webservicesRates']) {
                foreach ($data['webservicesRates'] as $item) {
                    $rateExternal = new RateExternal($item);
                    $this->addExternalRate($rateExternal);
                }
            }

            if (isset($data['internalRates']) && $data['internalRates']) {
                foreach ($data['internalRates'] as $item) {
                    $rateInternal = new RateInternal($item);
                    $this->addInternalRate($rateInternal);
                }
            }

            if (isset($data['customRates']) && $data['customRates']) {
                foreach ($data['customRates'] as $item) {
                    $rateCustom = new RateCustom($item);
                    $this->addCustomRate($rateCustom);
                }
            }

            if (isset($data['backupRates']) && $data['backupRates']) {
                foreach ($data['backupRates'] as $item) {
                    $rateBackup = new RateBackup($item);
                    $this->addBackupRate($rateBackup);
                }
            }

            if (isset($data['shipments']) && $data['shipments']) {
                foreach ($data['shipments'] as $item) {
                    $shipment = new Shipment($item);
                    $this->addShipment($shipment);
                }
            }

            if (isset($data['order']) && $data['order']) {
                $order = new Order($data['order']);
                $this->setOrder($order);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return Address
     */
    public function getAddressFrom(): Address
    {
        return $this->addressFrom;
    }

    /**
     *
     * @param Address $addressFrom
     * @return self
     */
    public function setAddressFrom(Address $addressFrom): self
    {
        $this->addressFrom = $addressFrom;
        return $this;
    }

    /**
     *
     * @return Address
     */
    public function getAddressTo(): Address
    {
        return $this->addressTo;
    }

    /**
     *
     * @param Address $addressTo
     * @return self
     */
    public function setAddressTo(Address $addressTo): self
    {
        $this->addressTo = $addressTo;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     *
     * @param Item $item
     * @return self
     */
    public function addItem(Item $item): self
    {
        $this->items[] = $item;
        return $this;
    }

    /**
     *
     * @param array $items
     * @return self
     */
    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getCustomRates(): array
    {
        return $this->customRates;
    }

    /**
     *
     * @param RateCustom $customRate
     * @return self
     */
    public function addCustomRate(RateCustom $customRate): self
    {
        $this->customRates[] = $customRate;
        return $this;
    }

    /**
     *
     * @param array $customRates
     * @return self
     */
    public function setCustomRates(array $customRates): self
    {
        $this->customRates = $customRates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getBackupRates(): array
    {
        return $this->backupRates;
    }

    /**
     *
     * @param RateBackup $rateBackup
     * @return self
     */
    public function addBackupRate(RateBackup $rateBackup): self
    {
        $this->backupRates[] = $rateBackup;
        return $this;
    }

    /**
     *
     * @param array $backupRates
     * @return self
     */
    public function setBackupRates(array $backupRates): self
    {
        $this->backupRates = $backupRates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getExternalRates(): array
    {
        return $this->externalRates;
    }

    /**
     *
     * @param RateExternal $externalRate
     * @return self
     */
    public function addExternalRate(RateExternal $externalRate): self
    {
        $this->externalRates[] = $externalRate;
        return $this;
    }

    /**
     *
     * @param array $externalRates
     * @return self
     */
    public function setExternalRates(array $externalRates): self
    {
        $this->externalRates = $externalRates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getInternalRates(): array
    {
        return $this->internalRates;
    }

    /**
     *
     * @param RateInternal $internalRate
     * @return self
     */
    public function addInternalRate(RateInternal $internalRate): self
    {
        $this->internalRates[] = $internalRate;
        return $this;
    }

    /**
     *
     * @param array $internalRates
     * @return self
     */
    public function setInternalRates(array $internalRates): self
    {
        $this->internalRates = $internalRates;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getShipments(): array
    {
        return $this->shipments;
    }

    /**
     *
     * @param Shipment $shipment
     * @return self
     */
    public function addShipment(Shipment $shipment): self
    {
        $this->shipments[] = $shipment;
        return $this;
    }

    /**
     *
     * @param array $shipments
     * @return self
     */
    public function setShipments(array $shipments): self
    {
        $this->shipments = $shipments;
        return $this;
    }

    /**
     *
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     *
     * @param Order $order
     * @return self
     */
    public function setOrder(Order $order): self
    {
        $this->order = $order;
        return $this;
    }
}