<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model;

use DateTime;

abstract class Model
{
    /**
     *
     * @return array
     */
    public function __toArray(): array
    {        
        $result = [];
        foreach ($this as $key => $value)
        {
            if ($value !== null) {
                if (is_object($value)) {
                    if ($value instanceof DateTime) {
                        $result[$key] = $value->format('Y-m-d H:i:s');
                    } else {
                        $result[$key] = $value->__toArray();
                    }
                } elseif(is_array($value)) {
                    $array = [];
                    foreach ($value as $index => $val) {
                        if ($val !== null) {
                            if (is_object($val)) {
                                if ($val instanceof DateTime) {
                                    $result[$key] = $val->format('Y-m-d H:i:s');
                                } else {
                                    $result[$key] = $val->__toArray();
                                }
                            } else {
                                $array[$index] = $val;
                            }
                        }
                    }
                    $result[$key] = $array;
                } else {
                    $result[$key] = $value;
                }
            }
        }
        return $result;
    }
}