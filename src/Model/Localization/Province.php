<?php
/*
 * @since 1.3.4
 * @copyright Copyright (C) 2023 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Localization;

use WorldOptions\Model\Model;

final class Province extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $isoCode
     */
    protected string $isoCode;

    /**
     * @var Country $country
     */
    protected Country $country;

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            $this->setIsoCode($data['isoCode']);

            if (isset($data['country']) && $data['country']) {
                $country = new Country($data['country']);
                $this->setCountry($country);
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getIsoCode(): string
    {
        return $this->isoCode;
    }

    /**
     *
     * @param string $isoCode
     * @return self
     */
    public function setIsoCode(string $isoCode): self
    {
        $this->isoCode = $isoCode;
        return $this;
    }

    /**
     *
     * @return Country
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function setCountry(Country $country): self
    {
        $this->country = $country;
        return $this;
    }
}