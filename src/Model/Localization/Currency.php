<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Localization;

use WorldOptions\Model\Model;

final class Currency extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $symbol
     */
    protected ?string $symbol;

    /**
     * @var string $code
     */
    protected string $code;

    /**
     * @var array $countries
     */
    protected array $countries = [];

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            $this->setSymbol($data['symbol']);
            $this->setCode($data['code']);

            if (isset($data['countries']) && $data['countries']) {
                foreach ($data['countries'] as $item) {
                    $country = new Country($item);
                    $this->addCountry($country);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getSymbol(): ?string
    {
        return $this->symbol;
    }

    /**
     *
     * @param string $symbol
     * @return self
     */
    public function setSymbol(?string $symbol): self
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     *
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    /**
     *
     * @param Country $country
     * @return self
     */
    public function addCountry(Country $country): self
    {
        $this->country[] = $country;
        return $this;
    }

    /**
     *
     * @param array $countries
     * @return self
     */
    public function setCountries(array $countries): self
    {
        $this->countries = $countries;
        return $this;
    }
}