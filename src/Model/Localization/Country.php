<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions\Model\Localization;

use WorldOptions\Model\Model;

final class Country extends Model
{
    /**
     * @var int $id
     */
    protected int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var string $code
     */
    protected string $code;

    /**
     * @var Currency $currency
     */
    protected ?Currency $currency;

    /**
     * @var array $zones
     */
    protected array $zones = [];

    /**
     * @var array $provinces
     */
    protected array $provinces = [];

    /**
     * @param ?array $data
     * 
     */
    public function __construct(?array $data = [])
    {
        if ($data) {
            $this->setId($data['id']);
            $this->setName($data['name']);
            $this->setCode($data['code']);

            if (isset($data['currency']) && $data['currency']) {
                $currency = new Currency($data['currency']);
                $this->setCurrency($currency);
            }

            if (isset($data['zones']) && $data['zones']) {
                foreach ($data['zones'] as $item) {
                    $zone = new Zone($item);
                    $this->addZone($zone);
                }
            }

            if (isset($data['provinces']) && $data['provinces']) {
                foreach ($data['provinces'] as $item) {
                    $province = new Province($item);
                    $this->addProvince($province);
                }
            }
        }
    }

    /**
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return self
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     *
     * @param string $code
     * @return self
     */
    public function setCode(string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCurrency(): ?Currency
    {
        return $this->currency;
    }

    /**
     *
     * @param string $currency
     * @return self
     */
    public function setCurrency(?Currency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getZones(): array
    {
        return $this->zones;
    }

    /**
     *
     * @param Zone $zone
     * @return self
     */
    public function addZone(Zone $zone): self
    {
        $this->zones[] = $zone;
        return $this;
    }

    /**
     *
     * @param array $zones
     * @return self
     */
    public function setZones(array $zones): self
    {
        $this->zones = $zones;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getProvinces(): array
    {
        return $this->provinces;
    }

    /**
     *
     * @param Province $province
     * @return self
     */
    public function addProvince(Province $province): self
    {
        $this->provinces[] = $province;
        return $this;
    }

    /**
     *
     * @param array $provinces
     * @return self
     */
    public function setProvinces(array $provinces): self
    {
        $this->provinces = $provinces;
        return $this;
    }
}