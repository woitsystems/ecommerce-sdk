<?php
/*
 * @since 1.0.0
 * @copyright Copyright (C) 2022 TheIFactory. All rights reserved.
 * @website https://theifactory.com/
 * @author Arkadiusz Tobiasz
 * @email arkadiusz.tobiasz@theifactory.com
 */

namespace WorldOptions;

use Http\Client\Common\HttpMethodsClient;
use Http\Client\Common\HttpMethodsClientInterface;
use Http\Client\Common\Plugin;
use Http\Client\Common\PluginClientFactory;
use Http\Discovery\HttpClientDiscovery;
use Http\Discovery\Psr17FactoryDiscovery;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;

final class ClientBuilder
{
    /**
     * @var ClientInterface $httpClient
     */
    private ClientInterface $httpClient;
    
    /**
     * @var RequestFactoryInterface $requestFactoryInterface
     */
    private RequestFactoryInterface $requestFactoryInterface;
    
    /**
     * @var StreamFactoryInterface $streamFactoryInterface
     */
    private StreamFactoryInterface $streamFactoryInterface;
    
    /**
     * @var array $plugins
     */
    private array $plugins = [];
    
    /**
     * @param ClientInterface $httpClient
     * @param RequestFactoryInterface $requestFactoryInterface
     * @param StreamFactoryInterface $streamFactoryInterface
     * 
     */
    public function __construct(
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactoryInterface = null,
        StreamFactoryInterface $streamFactoryInterface = null
    ) {
        $this->httpClient = $httpClient ?: HttpClientDiscovery::find();
        $this->requestFactoryInterface = $requestFactoryInterface ?: Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactoryInterface = $streamFactoryInterface ?: Psr17FactoryDiscovery::findStreamFactory();
    }
    
    /**
     * 
     * @param Plugin $plugin
     * 
     */
    public function addPlugin(Plugin $plugin): void
    {
        $this->plugins[] = $plugin;   
    }
    
    /**
     * 
     * @return HttpMethodsClientInterface
     * 
     */
    public function getHttpClient(): HttpMethodsClientInterface
    {
        $pluginClient = (new PluginClientFactory())->createClient($this->httpClient, $this->plugins);
        
        return new HttpMethodsClient(
            $pluginClient,
            $this->requestFactoryInterface,
            $this->streamFactoryInterface
        );
    }
}