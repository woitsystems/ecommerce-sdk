# World Options SDK for PHP - Version 1

The **World Options SDK for PHP** makes it easy for developers to access [World Options Web Services][wo] in their PHP code, and build robust applications and software. You can get started in minutes by [installing the SDK through Composer](#Getting-Started) or by downloading a single zip or phar file from our [latest release][latest-release].

Jump To:
* [Getting Started](#Getting-Started)
* [Quick Examples](#Quick-Examples)
* [Getting Help](#Getting-Help)

## Getting Started

1. **Sign up for World Options** – Before you begin, you need to
   sign up for an World Options account and retrieve your [World Options credentials][wo].
2. **Minimum requirements** – To run the SDK, your system will need to meet the
   minimum requirements, including having **PHP >= 7.1**.
   We highly recommend having it compiled with the cURL extension and cURL
   7.16.2+ compiled with a TLS backend (e.g., NSS or OpenSSL).
3. **Install the SDK** – Using [Composer] is the recommended way to install the
   World Options SDK for PHP. The SDK is available via [Packagist] under the
   [`worldoptions/php-sdk`][install-packagist] package. If Composer is installed globally on your system, you can run the following in the base directory of your project to add the SDK as a dependency:
   ```
   composer require worldoptions/php-sdk
   ```
4. **Using the SDK** – The best way to become familiar with how to use the SDK
   is to read the [Documentation][docs-guide]. The
   [Quick Examples](#Quick-Examples) will help you become familiar with
   the basic concepts.

## Quick Examples

### World Options API client

```php
<?php
// Require the Composer autoloader.
require 'vendor/autoload.php';

use WorldOptions\WorldOptions as WorldOptionsApi;

// Instantiate an World Options API client.
$siteUrl = 'http://example.com'; 
$username = 'WOTest'; 
$meternumber = 'ddffg3rfdweq22rffeiwijew'; // You can check it on Customer Portal -> Integrations Tab

$worldOptionsApi = new WorldOptionsApi($siteUrl, $username, $meternumber);
```

### Create Shop

```php
<?php
try {
    use WorldOptions\WorldOptions as WorldOptionsApi;
    use WorldOptions\Model\Shop;
    use WorldOptions\Model\Core\Customer;
    use WorldOptions\Model\Localization\Country;

    $siteUrl = 'http://example.com';
    
    $api = new WorldOptionsApi($siteUrl);

    $customer = new Customer();
    $customer->setUsername("WOTest");
    $customer->setPassword("WOTestPSW");
    $customer->setMeternumber("ddffg3rfdweq22rffeiwijew");

    $country = new Country();
    $country->setCode("GB");
    $customer->setCountry($country);

    $shop = new Shop();
    $shop->setUrl($url);
    $shop->setPlatform(Shop::PLATFORM_CUSTOM);
    $shop->setCustomer($customer);

    /**
     * @return WorldOptions\Model\Shop
     */
    $shop = $api->shops()->authorize($shop);
} catch (WorldOptions\Utils\ResponseException $e) {
    echo "There was an error when try creating shop.\n";
}
```

## Getting Help
If you have any problems, found a security issue, please contact the maintainers directly at [arkadiusz.tobiasz@theifactory.com](mailto:arkadiusz.tobiasz@theifactory.com).

[wo]: https://www.worldoptions.com
[latest-release]: https://bitbucket.org/woitsystems/ecommerce-sdk/downloads/?tab=tags
[install-packagist]: https://packagist.org/packages/worldoptions/php-sdk
[docs-guide]: https://ecommerce.worldoptions.com/api/docs?ui=re_doc